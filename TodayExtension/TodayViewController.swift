//
//  TodayViewController.swift
//  TodayExtension
//
//  Created by SeungHoon Jung on 2018. 4. 30..
//  Copyright © 2018년 SeungHoon Jung. All rights reserved.
//

import UIKitExtension
import NotificationCenter
import RealmSwift

// MARK: - TodayCell

class TodayCell: TableCell {
    @IBOutlet weak var profileView: UIImageView!
    @IBOutlet weak var genderView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var ageLabel: UILabel!
    @IBOutlet weak var sleepLabel: UILabel!
    @IBOutlet weak var feedingLabel: UILabel!
    @IBOutlet weak var diaperLabel: UILabel!
    @IBOutlet weak var selectedIndicator: UIImageView!
    
    var child: Child! {
        didSet {
            profileView.image = child.profile?.image ?? #imageLiteral(resourceName: "profile")
            genderView.image = child.gender == 0 ? #imageLiteral(resourceName: "male") : #imageLiteral(resourceName: "female")
            genderView.tintColor = child.gender == 0 ? .sub : .main
            selectedIndicator.isHidden = !child.isSelected
            selectedIndicator.tintColor = .heavyMain
            nameLabel?.text = child.name
            ageLabel?.text = child.age
            update(child.records.sorted(byKeyPath: "started", ascending: false))
        }
    }
    
    
    func update(_ records: Results<AnyRecord>) {
        let lastFeeding = records.filter("typeName = %@ OR typeName = %@ OR typeName = %@", "Breastfeeding", "Bottlefeeding", "Meal").first?.started
        let lastDiaper = records.filter("typeName = %@", "Diaper").first?.started
        let lastSleep = records.filter("typeName = %@", "Sleep").first?.started
        feedingLabel.text = DateFormat.string(from: lastFeeding)
        diaperLabel.text = DateFormat.string(from: lastDiaper)
        sleepLabel.text = DateFormat.string(from: lastSleep)
    }
}


// MARK: - Controller

class TodayViewController: UIViewController, NCWidgetProviding {
    
    // MARK: - Properties
    
    @IBOutlet weak var noDataLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var babies: Results<Child>!
    
    var numberOfCells: Int {
        guard let displayMode = extensionContext?.widgetActiveDisplayMode else {
            return 0
        }
        
        switch displayMode {
        case .compact: return babies.count
        case .expanded: return 2
        }
    }
    
    var heightForCells: CGFloat = 95.0
    
    
    // MARK: - Methods
        
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view from its nib.
        let realm = RealmManager.shared
        babies = realm.read(Child.self)
            .sorted(byKeyPath: "dob", ascending: false)
            .sorted(byKeyPath: "isSelected", ascending: false)
        
        switch babies.count {
        case 0:
            extensionContext?.widgetLargestAvailableDisplayMode = .compact
        case 1:
            extensionContext?.widgetLargestAvailableDisplayMode = .compact
            noDataLabel.text = ""
        default:
            extensionContext?.widgetLargestAvailableDisplayMode = .expanded
            noDataLabel.text = ""
        }
    }
    
    func widgetPerformUpdate(completionHandler: (@escaping (NCUpdateResult) -> Void)) {
        // Perform any setup necessary in order to update the view.
        
        // If an error is encountered, use NCUpdateResult.Failed
        // If there's no update required, use NCUpdateResult.NoData
        // If there's an update, use NCUpdateResult.NewData
        
        beaver.debug("widget perform update")
        completionHandler(NCUpdateResult.newData)
    }
    
    func widgetActiveDisplayModeDidChange(_ activeDisplayMode: NCWidgetDisplayMode, withMaximumSize maxSize: CGSize) {
        beaver.debug("widget active display mode did change")
        if activeDisplayMode == .compact {
            self.preferredContentSize = maxSize
            self.heightForCells = maxSize.height
        } else if activeDisplayMode == .expanded {
            self.preferredContentSize = CGSize(width: maxSize.width, height: 200)
            self.heightForCells = 100
        }
        
        self.tableView.reloadData()
    }
}

extension TodayViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return numberOfCells
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: TodayCell = tableView.dequeueCell()
        cell.child = babies[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return heightForCells
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        RealmManager.shared.select(child: self.babies[indexPath.row])
        let url = URL(fileURLWithPath: "babien://")
        extensionContext?.open(url, completionHandler: nil)
    }
}
