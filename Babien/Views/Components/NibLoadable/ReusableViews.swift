//
//  ReusableViews.swift
//  Babien
//
//  Created by SeungHoon Jung on 2018. 4. 5..
//  Copyright © 2018년 SeungHoon Jung. All rights reserved.
//

import UIKitExtension

// MARK: - Home

// MARK: - MenuButton
class MenuButton: CollectionCell {
    var type: RecordType! {
        didSet {
            shadow(offset: (0,0), radius: 3.5, opacity: 1, color: type.color)
            iconView.image = type.icon
        }
    }
    
    private lazy var iconView: UIImageView = {
        let view = UIImageView()
        view.tintColor = .darkGray
        return view
    }()
    
    override func setupView() {
        addSubview(iconView)
        iconView.anchorCenterToSuperview()
        iconView.sizeAnchor(30, 30)
        backgroundColor = .white
        rounded()
    }
}


// MARK: - EventCell
class EventCell: TableCell {
    @IBOutlet weak var indicator: UIView!
    @IBOutlet weak var iconView: UIImageView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    var record: AnyRecord! {
        didSet {
//            indicator.shadowColor = record.recordType.color
            indicator.borderColor = record.recordType.color
            indicator.borderWidth = 2.0
            indicator.removeShadow()
            iconView.image = record.recordType.icon
            timeLabel.text = DateFormat.string(from: record.started, type: .time)
            descriptionLabel.text = record.summary
        }
    }
}


// MARK: - StopwatchCell
class StopwatchCell: TableCell {
    @IBOutlet weak var okButton: UIButton!
    
    var record: AnyRecord!
    
    func start(stopwatch: Stopwatch) {
        RealmManager.shared.write {
            stopwatch.start()
        }
        registerTimer()
    }
    
    func stop(stopwatch: Stopwatch) {
        RealmManager.shared.write {
            stopwatch.stop()
        }
    }
    
    func registerTimer() {
        Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(stopwatch(_:)), userInfo: nil, repeats: true)
    }
    
    @objc func stopwatch(_ sender: Timer) {}
    
    @objc func finish() {
        RealmManager.shared.stopwatch(record)
    }
}

class OneStopwatchCell: StopwatchCell {
    @IBOutlet weak var displayLabel: UILabel!
    @IBOutlet weak var controlButton: UIButton!
    
    override var record: AnyRecord! {
        didSet {
            tintColor = record.recordType.color
            controlButton.addTarget(self, action: #selector(control(_:)), for: .touchUpInside)
            okButton.addTarget(self, action: #selector(finish), for: .touchUpInside)
            guard let sw: Stopwatch = record.stopwatches.first else { return }
            displayLabel.text = sw.displayTime(showHr: sw.max > 3600)
            if sw.isRunning {
                registerTimer()
                controlButton.setImage(#imageLiteral(resourceName: "pause"), for: .normal)
                backgroundColor = .lightSub
            } else {
                controlButton.setImage(#imageLiteral(resourceName: "play"), for: .normal)
                backgroundColor = .white
            }
        }
    }
    
    @objc func control(_ sender: UIButton) {
        guard let stopwatch: Stopwatch = record.stopwatches.first else { return }
        if stopwatch.isRunning { stop(stopwatch: stopwatch) }
        else { start(stopwatch: stopwatch) }
    }
    
    override func start(stopwatch: Stopwatch) {
        super.start(stopwatch: stopwatch)
        controlButton.setImage(#imageLiteral(resourceName: "pause"), for: .normal)
        backgroundColor = .lightSub
    }
    
    override func stop(stopwatch: Stopwatch) {
        super.stop(stopwatch: stopwatch)
        controlButton.setImage(#imageLiteral(resourceName: "play"), for: .normal)
        backgroundColor = .white
    }
    
    override func stopwatch(_ sender: Timer) {
        guard let sw = record.stopwatches.first else {
            return sender.invalidate()
        }
        if sw.isRunning {
            displayLabel.text = sw.displayTime(showHr: sw.max > 3600)
        } else {
            sender.invalidate()
        }
    }
}

// MARK: - TwoStopwatchCell
class TwoStopwatchCell: StopwatchCell {
    @IBOutlet weak var leftStopwatch: StopwatchView!
    @IBOutlet weak var rightStopwatch: StopwatchView!
    
    override var record: AnyRecord! {
        didSet {
            tintColor = record.recordType.color
            leftStopwatch.tintColor = tintColor
            rightStopwatch.tintColor = tintColor
            leftStopwatch.addTarget(self, action: #selector(leftControl), for: .touchUpInside)
            rightStopwatch.addTarget(self, action: #selector(rightControl), for: .touchUpInside)
            okButton.addTarget(self, action: #selector(finish), for: .touchUpInside)
            guard let sw0 = record.stopwatches.first, let sw1 = record.stopwatches.last else {
                return
            }
            if sw0.isRunning || sw1.isRunning { registerTimer() }
            leftStopwatch.text = sw0.displayTime(showHr: false)
            leftStopwatch.isOn = sw0.isRunning
            rightStopwatch.text = sw1.displayTime(showHr: false)
            rightStopwatch.isOn = sw1.isRunning
        }
    }
    
    @objc func leftControl() {
        let sw = record.stopwatches
        guard let sw0 = sw.first, let sw1 = sw.last else { return }
        if leftStopwatch.isOn {
            stop(stopwatch: sw0)
        } else {
            start(stopwatch: sw0)
            stop(stopwatch: sw1)
            rightStopwatch.isOn = false
        }
        
        leftStopwatch.toggle()
    }
    
    @objc func rightControl() {
        let sw = record.stopwatches
        guard let sw0 = sw.first, let sw1 = sw.last else { return }
        if rightStopwatch.isOn {
            stop(stopwatch: sw1)
        } else {
            start(stopwatch: sw1)
            stop(stopwatch: sw0)
            leftStopwatch.isOn = false
        }
        
        rightStopwatch.toggle()
    }
    
    override func stopwatch(_ sender: Timer) {
        let stopwatches = record.stopwatches
        guard let sw0 = stopwatches.first, let sw1 = stopwatches.last else {
            return sender.invalidate()
        }
        if sw0.isRunning || sw1.isRunning {
            leftStopwatch.text = sw0.displayTime(showHr: false)
            rightStopwatch.text = sw1.displayTime(showHr: false)
        } else {
            sender.invalidate()
        }
    }

}

// MARK: - Child

// MARK: - AddNewCell
class AddNewCell: TableCell {
    @IBOutlet weak var iconView: UIImageView!
    @IBOutlet weak var label: UILabel!
}


// MARK: - ChildListCell
class ChildListCell: TableCell {
    var child: Child! {
        didSet {
            profileView.image = child.profile?.image
            nameLabel.text = child.name
            ageLabel.text = child.age
            dobLabel.text = child.dobString
            
            tintColor = .main
            accessoryType = child.isSelected ? .checkmark : .none
        }
    }
    
    @IBOutlet weak var profileView: ProfileImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var ageLabel: UILabel!
    @IBOutlet weak var dobLabel: UILabel!
}


// MARK: - ChildFormCell
class ChildFormCell: TableCell {
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var iconView: UIImageView!
    
    var input: UIView! { didSet { setupInput() } }
    var icon: UIImage! { didSet { iconView.image = icon } }
    
    func setupInput() {
        containerView.addSubview(input)
        containerView.backgroundColor = .clear
        input.fillSuperview()
    }
}



// MARK: - Tracker

// MARK: - TrackerHeader
class TrackerHeader: ReusableView {
    var title: String! { didSet { titleLabel.text = title } }
    
    private let titleLabel: UILabel = {
        return UILabel(text: "", color: .darkGray, font: UIFont.systemFont(ofSize: 12))
    }()
    
    override func setupView() {
        backgroundColor = .white
        self.addSubview(titleLabel)
        titleLabel.anchorCenterYToSuperview()
        titleLabel.anchor(left: leftAnchor, 16)
        titleLabel.text = self.title
    }
}

// MARK: - TrackerCell
class TrackerCell: CollectionCell {
    var form: TrackerInput! { didSet { form.setup(self.contentView) } }
    
    override func setupView() { backgroundColor = .white }
}



// MARK: - Statistics

// MARK: - StatsCell
class StatsCell: TableCell, NibLoadable {
    // MARK: - Initializers
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        loadFromNib()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadFromNib()
    }
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var summaryLabel: UILabel!
    @IBOutlet weak var chartView: ChartControl!
    
    var data: StatsData! {
        didSet {
            titleLabel.text = data.title
            summaryLabel.text = data.summary
            chartView.data = data.data
        }
    }
    
    func initiate() {
        chartView.backgroundColor = .clear
    }
}
