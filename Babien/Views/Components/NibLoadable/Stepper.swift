//
//  Stepper.swift
//  Babien
//
//  Created by SeungHoon Jung on 2018. 3. 30..
//  Copyright © 2018년 SeungHoon Jung. All rights reserved.
//

import UIKitExtension


// MARK: - SuperClass

class Stepper: UIControl, NibLoadable {
    
    // MARK: - Initializers
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadFromNib()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadFromNib()
    }
    
    // MARK: - IBOutlets && UIComponents
    
    @IBOutlet weak var leftButton: UIButton!
    @IBOutlet weak var rightButton: UIButton!
    
    // MARK: - IBInspectables
    
    override var tintColor: UIColor! {
        didSet {
            leftButton.tintColor = tintColor
            rightButton.tintColor = tintColor
        }
    }
    
    @IBInspectable var textColor: UIColor = .darkGray
    
    
    // MARK: - Methods
    
    func initiate() {
        leftButton.addTarget(self, action: #selector(goPrev), for: .touchUpInside)
        rightButton.addTarget(self, action: #selector(goNext), for: .touchUpInside)
    }
    
    func updateView() { }
    
    
    // MARK: - IBActions && Selectors
    
    @objc func goPrev() { }
    
    @objc func goNext() { }
}


// MARK: - DateStepper

class DateStepper: Stepper {
    
    // MARK: - IBOutlets && UIComponents

    @IBOutlet weak var dateLabel: UILabel!
    
    // MARK: - IBInspectables
    
    override var textColor: UIColor {
        didSet {
            dateLabel.textColor = textColor
        }
    }
    
    // MARK: - Properties
    
    private var calendar: Calendar { return Calendar.current }
    
    var displayType: DateDisplayType = .daily { didSet { updateView() } }
    
    private var date: Date! { didSet { updateView() } }
    
    private var maxDate: Date!
    
    private var minDate: Date!
    
    var startDate: Date {
        switch displayType {
        case .daily: return date.begin
        case .weekly: return date.thisWeek.sunday
        case .monthly: return date.thisMonth.first
        }
    }
    
    var endDate: Date {
        switch displayType {
        case .daily: return date.end
        case .weekly: return date.thisWeek.saturday
        case .monthly: return date.thisMonth.last
        }
    }
    
    
    // MARK: - Methods
    
    override func updateView() {
        var text: String
        switch displayType {
        case .daily:
            text = DateFormat.string(from: date, type: .daily)
        case .weekly:
            let sat = DateFormat.string(from: startDate, type: .daily)
            let sun = DateFormat.string(from: endDate, type: .daily)
            text = "\(sat) - \(sun)"
        case .monthly:
            text = DateFormat.string(from: date, type: .monthly)
        }
        
        leftButton.isEnabled = calendar.compare(startDate, to: minDate, toGranularity: .day) == .orderedDescending
        rightButton.isEnabled = calendar.compare(endDate, to: maxDate, toGranularity: .day) == .orderedAscending
        
        dateLabel.text = text
        sendActions(for: .valueChanged)
    }
    
    func setDate(now: Date, max: Date, min: Date) {
        self.maxDate = max.end
        self.minDate = min.begin
        self.date = now
    }
    
    // MARK: - IBActions && Selectors
    
    override func goPrev() {
        var dc: DateComponents
        switch displayType {
        case .daily: dc = DateComponents(day: -1)
        case .weekly: dc = DateComponents(day: -7)
        case .monthly: dc = DateComponents(month: -1)
        }
        date = calendar.date(byAdding: dc, to: date)!
    }
    
    override func goNext() {
        var dc: DateComponents
        switch displayType {
        case .daily: dc = DateComponents(day: 1)
        case .weekly: dc = DateComponents(day: 7)
        case .monthly: dc = DateComponents(month: 1)
        }
        date = calendar.date(byAdding: dc, to: date)!
    }
}
