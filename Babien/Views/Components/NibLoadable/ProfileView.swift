//
//  ProfileView.swift
//  Babien
//
//  Created by SeungHoon Jung on 2018. 3. 28..
//  Copyright © 2018년 SeungHoon Jung. All rights reserved.
//

import UIKitExtension

class ProfileView: NibView {
    
    // MARK: - IBOutlets && Compoenents
    
    @IBOutlet weak var imageView: ProfileImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var genderView: UIImageView!
    @IBOutlet weak var ageLabel: UILabel!
    @IBOutlet weak var trackerButton: UIButton!
    @IBOutlet weak var stopwatchButton: UIButton!
    @IBOutlet weak var statsButton: UIButton!
    
    var buttons: [UIButton] { return [trackerButton, stopwatchButton, statsButton] }
    
    // MARK: - Computed Properties
    
    var image: UIImage? {
        get { return imageView.image }
        set { imageView.image = newValue }
    }
    
    var name: String? {
        get { return nameLabel.text }
        set { nameLabel.text = newValue }
    }
    
    var age: String? {
        didSet { ageLabel.text = age }
    }
    
    var gender: Int? {
        didSet {
            if gender == 0 {
                genderView.image = #imageLiteral(resourceName: "male")
                genderView.tintColor = .sub
                ageLabel.textColor = .main
            } else {
                genderView.image = #imageLiteral(resourceName: "female")
                genderView.tintColor = .main
                ageLabel.textColor = .sub
            }
        }
    }
    
    
    // MARK: - Methods
    
    override func initiate() {
        imageView.accessoryImage = #imageLiteral(resourceName: "refresh")
        
    }
    
    func addTargetWithImage(target: Any?, action: Selector, for events: UIControl.Event) {
        imageView.addTarget(target: target, action: action, for: events)
    }
    
    func addTargetWithButtons(target: Any?, action: Selector, for events: UIControl.Event) {
        buttons.forEach { $0.addTarget(target, action: action, for: events) }
    }
    
    
}
