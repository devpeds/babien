//
//  RecentView.swift
//  Babien
//
//  Created by SeungHoon Jung on 2018. 3. 29..
//  Copyright © 2018년 SeungHoon Jung. All rights reserved.
//

import UIKitExtension

class RecentView: NibView {
    
    // MARK: - @IBOutlets && Components
    
    @IBOutlet weak var feedingLabel: UILabel!
    @IBOutlet weak var diaperLabel: UILabel!
    @IBOutlet weak var sleepLabel: UILabel!
    
    func update(lastFeeding: Date?, lastDiaper: Date?, lastSleep: Date?) {
        feedingLabel.text = DateFormat.string(from: lastFeeding)
        diaperLabel.text = DateFormat.string(from: lastDiaper)
        sleepLabel.text = DateFormat.string(from: lastSleep)
    }
}
