//
//  StopwatchControl.swift
//  Babien
//
//  Created by SeungHoon Jung on 2018. 4. 5..
//  Copyright © 2018년 SeungHoon Jung. All rights reserved.
//

import UIKitExtension

class StopwatchView: UIControl, NibLoadable {
    
    // MARK: - Initializers
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadFromNib()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadFromNib()
    }
    
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var displayLabel: UILabel!

    // MARK: - IBInspectables
    
    @IBInspectable var title: String? {
        get { return titleLabel.text }
        set { titleLabel.text = newValue?.local }
    }
    
    @IBInspectable var text: String? {
        get { return displayLabel.text }
        set { displayLabel.text = newValue }
    }
    
    // MARK: - Properties
    
    var isOn: Bool = false {
        didSet {
            contentView.backgroundColor = isOn ? .lightSub : .white
        }
    }
    
    
    // MARK: - Methods
    
    func initiate() {
        backgroundColor = .white
    }
    
    func toggle() { isOn = !isOn }
    
}
