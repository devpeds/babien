//
//  ChartViewWrapper.swift
//  Babien
//
//  Created by SeungHoon Jung on 2018. 3. 31..
//  Copyright © 2018년 SeungHoon Jung. All rights reserved.
//

import UIKitExtension
import Charts


// MARK: - ChartControlDelegate

@objc protocol ChartControlDelegate: class {
    func chartType() -> ChartControl.ChartType
    func setup(chart: ChartViewBase, data: ChartData?)
    
    @objc optional func animateDuration() -> TimeInterval
}


// MARK: - ChartControl

class ChartControl: UIView {
    
    @objc enum ChartType: Int {
        case bar, line
    }
    
    
    // MARK: - Properties
    
    var type: ChartType = .bar { didSet { createChart() } }
    
    var chart: BarLineChartViewBase! { didSet { addChart() } }
    
    var data: ChartData? { didSet { updateChart() } }
    
    weak var delegate: ChartControlDelegate? { didSet { updateChart() } }
    
    
    // MARK: - Methods
    
    private func createChart() {
        switch type {
        case .bar: chart = BarChartView()
        case .line: chart = LineChartView()
        }
    }
    
    private func addChart() {
        backgroundColor = .white
        removeAll()
        addSubview(chart)
        chart.fillSuperview()
    }
    
    private func updateChart() {
        guard let d = delegate else { return }
        type = d.chartType()
        d.setup(chart: chart, data: data)
        
        guard let t = d.animateDuration?() else { return }
        chart.animate(xAxisDuration: t, yAxisDuration: t)
    }

}
