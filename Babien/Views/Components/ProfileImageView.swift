//
//  ProfileView.swift
//  Babien
//
//  Created by SeungHoon Jung on 2018. 3. 19..
//  Copyright © 2018년 SeungHoon Jung. All rights reserved.
//

import UIKitExtension

class ProfileImageView: View {
    
    // MARK: - Initializers
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupView()
    }
    
    
    // MARK: - Properties
    
    var image: UIImage? {
        didSet {
            let img = (self.image == nil) ? #imageLiteral(resourceName: "profile") : self.image
            self.imageView.setImage(img, for: .normal)
        }
    }
    
    var accessoryImage: UIImage? {
        didSet {
            accessoryView.isHidden = accessoryImage == nil
            accessoryView.setImage(accessoryImage, for: .normal)
        }
    }
    
    // MARK: - IBOutlets && Properties
    
    private lazy var imageView: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "profile"), for: .normal)
        return button
    }()
    
    private lazy var accessoryView: UIButton = {
        let button = UIButton()
        button.backgroundColor = .white
        button.tintColor = .darkGray
        button.setImage(#imageLiteral(resourceName: "camera"), for: .normal)
        return button
    }()
    
    
    // MARK: - Methods
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        setupView()
        imageView.backgroundColor = .lightGray
    }
    
    private func setupView(image: UIImage? = nil, accessoryImg: UIImage? = nil) {
        self.image = image
        self.accessoryImage = accessoryImg
        createImageView()
        createAccessoryView()
    }
    
    private func createImageView() {
        backgroundColor = .clear
        addSubview(imageView)
        let rec = layer.frame
        imageView.frame = CGRect(x: 0, y: 0, width: rec.width, height: rec.height)
        imageView.rounded(clipsToBounds: true)
    }
    
    private func createAccessoryView() {
        addSubview(accessoryView)
        let frame = self.layer.frame
        let rec = (width: frame.width/4, height: frame.height/4)
        let pos = (x: frame.width - rec.width, y: frame.height - rec.height)
        accessoryView.frame = CGRect(x: pos.x, y: pos.y, width: rec.width, height: rec.height)
        accessoryView.rounded(clipsToBounds: true)
    }
    
    
    func addTarget(target: Any?, action: Selector, for events: UIControl.Event) {
        imageView.addTarget(target, action: action, for: events)
        accessoryView.addTarget(target, action: action, for: events)
    }
    
    func removeTarget(target: Any?, action: Selector, for events: UIControl.Event) {
        imageView.removeTarget(target, action: action, for: events)
        accessoryView.removeTarget(target, action: action, for: events)
    }
    
}
