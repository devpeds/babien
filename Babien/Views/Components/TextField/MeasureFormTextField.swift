//
//  MeasureFormTextField.swift
//  Babien
//
//  Created by SeungHoon Jung on 2018. 3. 31..
//  Copyright © 2018년 SeungHoon Jung. All rights reserved.
//

import UIKitExtension

// MARK: - MeasureFormTextFieldDelegate

protocol MeasureFormTextFieldDelegate: class {
    func displayText(value: Float) -> String
}



// MARK: - MeasureFormTextField

class MeasureFormTextField: FormTextField {
    init(value: Float, isInteger: Bool, max: Float, unit: String, placeholder: String? = nil, frame: CGRect = .zero) {
        super.init(inputType: isInteger ? .integer : .float, placeholder: placeholder, frame: frame)
        self.value = value
        self.isInteger = isInteger
        self.max = max
        self.unit = unit
        self.text = String(format: isInteger ? "%0.0f" : "%0.1f", value)
        if value == 0 { self.text = "0" }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.inputType = .integer
        self.text = String(format: "%0.0f", value)
    }
    
    
    // MARK: - Properties
    
    private(set) var value: Float = 0 { didSet { sendActions(for: .valueChanged) } }
    
    private var max: Float = 100
    
    private var isInteger: Bool = false
    
    // MARK: - IBInspectables
    
    @IBInspectable var unit: String? { didSet { updateLabel() } }
    
    override var rightImage: UIImage? { didSet { updateLabel() } }
    
    override var font: UIFont? { didSet { updateLabel() } }
    
    override var textColor: UIColor? { didSet { updateLabel() } }
    
    // MARK: - Methods
    
    private func updateLabel() {
        if let unit = self.unit {
            addLabel(withText: unit, on: .right)
        } else {
            rightView = nil
            rightViewMode = .never
        }
    }
    
    func update(value: Float) {
        self.value = value
    }
    
    func convertToNumber(range: NSRange, replacement: String) -> Bool {
        let text = self.text ?? ""
        var newText = (text as NSString).replacingCharacters(in: range, with: replacement)
        
        let formatter = NumberFormatter()
        formatter.allowsFloats = !isInteger
        formatter.minimumIntegerDigits = 1
        formatter.maximumFractionDigits = 1
        formatter.roundingMode = .down
        formatter.maximum = NSNumber(value: max)
        
        if replacement == "." {
            if text.isEmpty {
                self.text = "0"
                value = 0
            }
            return !text.contains(".")
        }
        if replacement == "" { newText = "" }
        
        if newText.isEmpty {
            self.text = "0"
            value = 0
        }

        if let number = formatter.number(from: newText) {
            self.text = formatter.string(from: number)
            value = formatter.number(from: self.text!)?.floatValue ?? number.floatValue
        }
        
        return false
    }
}
