//
//  DateFormTextField.swift
//  Babien
//
//  Created by SeungHoon Jung on 2018. 3. 24..
//  Copyright © 2018년 SeungHoon Jung. All rights reserved.
//

import UIKitExtension

// MARK: - DateFormTextFieldDelegate

protocol DateFormTextFieldDelegate: class {
    func setDatePicker(_ datePicker: UIDatePicker)
    
    func displayText(textField: FormTextField, datePicker: UIDatePicker) -> String
}


// MARK: - DateFormTextField

class DateFormTextField: FormTextField {
    init(date: Date, placeholder: String? = nil, frame: CGRect = .zero) {
        super.init(inputType: .date, placeholder: placeholder, frame: frame)
        self.setDatePicker(date: date)
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.inputType = .date
        self.setDatePicker(date: Date())
    }
    
    
    // MARK: - Properties
    
    var date: Date {
        get { return datePicker.date }
        set { datePicker.date = newValue }
    }
    
    var datePicker: UIDatePicker {
        if let dp = self.inputView as? UIDatePicker { return dp }
        return UIDatePicker()
    }
    
    var dateDelegate: DateFormTextFieldDelegate? {
        didSet {
            dateDelegate?.setDatePicker(self.datePicker)
        }
    }
    
    
    
    // MARK: - Methods
    
    private func setDatePicker(date: Date) {
        self.date = date
        self.datePicker.addTarget(self, action: #selector(displayText), for: .valueChanged)
    }
    
    func setRange(from min: Date?, to max: Date?) {
        datePicker.minimumDate = min
        datePicker.maximumDate = max
    }
    
    
    @objc func displayText() {
        text = dateDelegate?.displayText(textField: self, datePicker: datePicker)
    }
    
}
