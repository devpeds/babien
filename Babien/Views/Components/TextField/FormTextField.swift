//
//  FormTextField.swift
//  Babien
//
//  Created by SeungHoon Jung on 2018. 3. 23..
//  Copyright © 2018년 SeungHoon Jung. All rights reserved.
//

import UIKitExtension

// MARK: - TextFieldInputType

enum TextFieldInputType {
    case name
    case email
    case password
    case date
    case integer
    case float
    case text
}

// MARK: - FormTextFieldLayout

protocol FormTextFieldLayout {
    func customize(_ textField: FormTextField)
}


// MARK: - FormTextField

class FormTextField: TextField {
    init(inputType: TextFieldInputType = .text, placeholder: String? = nil, frame: CGRect = .zero) {
        super.init(frame: frame)
        self.setTextField(inputType: inputType, placeholder: placeholder)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    // MARK: - Properties
    
    var inputType: TextFieldInputType = .text { didSet { setKeyboard() } }
    
    var layout: FormTextFieldLayout? { didSet { layout?.customize(self) } }
    
    // MARK: - Methods
    
    private func setTextField(inputType: TextFieldInputType, placeholder: String?) {
        self.inputType = inputType
        self.placeholder = placeholder
    }
    
    private func setKeyboard() {
        self.autocorrectionType = .no
        self.keyboardType = .default
        
        switch self.inputType {
        case .email:
            self.keyboardType = .emailAddress
        case .password:
            self.isSecureTextEntry = true
        case .date:
            self.inputView = UIDatePicker()
        case .integer:
            self.keyboardType = .numberPad
        case .float:
            self.keyboardType = .decimalPad
        default:
            break
        }
    }
    
}
