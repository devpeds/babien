//
//  Radio.swift
//  Babien
//
//  Created by SeungHoon Jung on 2018. 3. 31..
//  Copyright © 2018년 SeungHoon Jung. All rights reserved.
//

import UIKitExtension

// MARK: - Superclass

class RadioBase: UIControl {
    override var cornerRadius: CGFloat {
        didSet { buttons.forEach { $0.cornerRadius = cornerRadius } }
    }
    
    internal var buttons: [UIButton] = []
    internal var numberOfButtons: Int { return buttons.count }
    
    var selectedItem: Int = 0 { didSet { update(old: oldValue, new: selectedItem) } }
    
    
    internal func setup() {
        setupButtons()
        
        let stackView = UIStackView(arrangedSubviews: buttons)
        addSubview(stackView)
        stackView.fillSuperview()
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        stackView.spacing = 10.0
    }
    
    
    internal func addAction(to button: UIButton) {
        button.addTarget(self, action: #selector(buttonDidTapped(_:)), for: .touchUpInside)
    }
    
    internal func setupButtons() {}
    
    internal func updateView(old: Int, new: Int) {}
    
    private func update(old: Int, new: Int) {
        updateView(old: old, new: new)
        sendActions(for: .valueChanged)
    }
    
    @objc private func buttonDidTapped(_ sender: UIButton) {
        selectedItem = buttons.index(of: sender)!
    }
}


// MARK: - Radio

class Radio: RadioBase {
    convenience init(titles: [String], frame: CGRect = .zero) {
        self.init(frame: frame)
        setItems(titles: titles)
    }
    
    convenience init(titles: [String], images: [UIImage], frame: CGRect = .zero) {
        self.init(frame: frame)
        setItems(titles: titles, images: images)
    }
    
    private struct ButtonItem {
        let title: String
        let image: UIImage?
    }
    
    private var items: [ButtonItem] = []
    
    func setItems(titles: [String]) {
        titles.forEach { items.append(ButtonItem(title: $0, image: nil)) }
        setup()
    }
    
    func setItems(titles: [String], images: [UIImage]) {
        for i in 0..<titles.count { items.append(ButtonItem(title: titles[i], image: images[i])) }
        setup()
    }
    
    override func setupButtons() {
        self.buttons = items.map { (item) -> UIButton in
            let btn = UIButton()
            btn.tintColor = .main
            btn.backgroundColor = .white
            btn.border(radius: self.cornerRadius, width: 2, color: .main)
            btn.setTitle(item.title, for: .normal)
            btn.setTitleColor(.main, for: .normal)
            btn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
            btn.setImage(item.image, for: .normal)
            addAction(to: btn)
            return btn
        }
        
        buttons[selectedItem].tintColor = .white
        buttons[selectedItem].backgroundColor = .main
        buttons[selectedItem].setTitleColor(.white, for: .normal)
    }
    
    override func updateView(old: Int, new: Int) {
        buttons[old].tintColor = .main
        buttons[old].backgroundColor = .white
        buttons[old].setTitleColor(.main, for: .normal)
        
        buttons[new].tintColor = .white
        buttons[new].backgroundColor = .main
        buttons[new].setTitleColor(.white, for: .normal)
    }
}


// MARK: - ColorSelector

class ColorSelector: RadioBase {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    private let colors: [UIColor] = [
        [1.00, 0.86, 0.22], [0.08, 0.24, 0.03], [0.62, 0.30, 0.00], [0.69, 0.06, 0.06],
        [0.22, 0.10, 0.09], [0.25 ,0.68 ,0.14], [0.85, 0.85, 0.85]]
        .map { UIColor(red: $0[0], green: $0[1], blue: $0[2], alpha: 1) }
    
    var selectedColor: UIColor { return colors[selectedItem] }
    
    override func setupButtons() {
        buttons = colors.map { (color) -> UIButton in
            let btn = UIButton()
            btn.backgroundColor = color
            btn.tintColor = color
            addAction(to: btn)
            return btn
        }
        
        buttons[selectedItem].backgroundColor = colors[selectedItem].withAlphaComponent(0.3)
        buttons[selectedItem].setImage(#imageLiteral(resourceName: "check"), for: .normal)
    }
    
    override func updateView(old: Int, new: Int) {
        buttons[old].backgroundColor = colors[old]
        buttons[old].setImage(nil, for: .normal)
        buttons[new].backgroundColor = colors[new].withAlphaComponent(0.3)
        buttons[new].setImage(#imageLiteral(resourceName: "check"), for: .normal)
    }
}
