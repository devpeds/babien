//
//  Records.swift
//  Babien
//
//  Created by SeungHoon Jung on 2018. 3. 16..
//  Copyright © 2018년 SeungHoon Jung. All rights reserved.
//

import Foundation
import RealmSwift


// MARK: - AnyRecord

class AnyRecord: BaseWrapper, Wrappable {
    
    convenience init(record: Record) {
        self.init(record, type: type(of: record))
        self.started = record.started
    }
    
    // MARK: - Properties
    
    @objc dynamic var started: Date = Date()
    @objc dynamic var numberOfStopwatches: Int = 0
    
    let stopwatches = List<Stopwatch>()
    
    var recordType: RecordType { return RecordType.init(title: typeName)! }
    
    var summary: String { return value.summary }
    
    
    // MARK: Statics
    
    static let supportedClasses: [Record.Type] =
        [Sleep.self, Breastfeeding.self, Bottlefeeding.self, Pump.self, Meal.self, Diaper.self, Growth.self]
    
    static var lookup: [String : Record.Type] = { return AnyRecord.createLookup() }()
    
    override static func indexedProperties() -> [String] {
        return ["typeName", "started", "numberOfStopwatches"]
    }
    
}


// MARK: - Record(SuperClass)

class Record: BaseObject {
    
    @objc dynamic var started: Date = Date()
    @objc dynamic var memo: String?
    
    var summary: String { return "" }
    
    override static func indexedProperties() -> [String] { return ["started"] }
}


// MARK: - TypeSelectable
protocol TypeSelectable {
    static var typeNames: [String] { get }
    
    var typeIndex: Int { get }
    var typeName: String { get }
}

extension TypeSelectable where Self: Record {
    var typeName: String { return Self.typeNames[typeIndex].local }
}


// MARK: - Sleep

class Sleep: Record, TypeSelectable {
    convenience init(started: Date, duration: Float, type: Int, memo: String?) {
        self.init()
        self.started = started
        self.duration = duration
        self.typeIndex = type
        self.memo = memo
        self.id = "SLP-\(self.id)"
    }
    
    static let typeNames: [String] = "sleep_sum".list(count: 2)
    
    @objc dynamic var duration: Float = 0
    @objc dynamic var typeIndex: Int = 0
    
    override var summary: String {
        return String(format: "sum_sleep".local, typeName, DateFormat.string(from: duration))
    }
}


// MARK: - Breastfeeding

class Breastfeeding: Record {
    convenience init(started: Date, left: Float, right: Float, memo: String?) {
        self.init()
        self.started = started
        self.durationLeft = left
        self.durationRight = right
        self.memo = memo
        self.id = "BRF-\(self.id)"
    }
    
    @objc dynamic var durationLeft: Float = 0
    @objc dynamic var durationRight: Float = 0
    
    override var summary: String {
        return String(format: "sum_breast".local,
                      DateFormat.string(from: durationLeft),
                      DateFormat.string(from: durationRight))
    }
}

// MARK: - Bottlefeeding

class Bottlefeeding: Record, TypeSelectable {
    convenience init(started: Date, duration: Float, type: Int, amount: Float, memo: String?) {
        self.init()
        self.started = started
        self.duration = duration
        self.typeIndex = type
        self.amount = amount
        self.memo = memo
        self.id = "BOF-\(self.id)"
    }
    
    static let typeNames: [String] = "bottle_type".list(count: 3)
    
    @objc dynamic var duration: Float = 0
    @objc dynamic var typeIndex: Int = 0
    @objc dynamic var amount: Float = 0
    @objc dynamic var unitIndex: Int = VolumeUnit.storedType.rawValue
    
    override var summary: String {
        let unit = VolumeUnit.init(rawValue: unitIndex)!
        let mf = MeasureFormat.volume(unit: unit)
        return String(format: "sum_bottle".local,
                      typeName, mf.string(amount), DateFormat.string(from: duration))
    }
}


// MARK: - Pump

class Pump: Record, TypeSelectable {
    convenience init(started: Date, left: Float, right: Float, type: Int, amount: Float, memo: String?) {
        self.init()
        self.started = started
        self.durationLeft = left
        self.durationRight = right
        self.typeIndex = type
        self.amount = amount
        self.memo = memo
        self.id = "PMP-\(self.id)"
    }
    
    static var typeNames: [String] = "pump_type".list(count: 3)
    
    @objc dynamic var durationLeft: Float = 0
    @objc dynamic var durationRight: Float = 0
    @objc dynamic var typeIndex: Int = 0
    @objc dynamic var amount: Float = 0
    @objc dynamic var unitIndex: Int = VolumeUnit.storedType.rawValue
    
    var duration: Float { return durationLeft + durationRight }
    
    override var summary: String {
        let unit = VolumeUnit.init(rawValue: unitIndex)!
        let mf = MeasureFormat.volume(unit: unit)
        return String(format: "sum_pump".local,
                      mf.string(amount),
                      DateFormat.string(from: duration),
                      DateFormat.string(from: durationLeft),
                      DateFormat.string(from: durationRight))
    }
}


// MARK: - Meal

class Meal: Record {
    convenience init(started: Date, duration: Float, amount: Float, memo: String?) {
        self.init()
        self.started = started
        self.duration = duration
        self.amount = amount
        self.memo = memo
        self.id = "MEA-\(self.id)"
    }
    
    @objc dynamic var duration: Float = 0
    @objc dynamic var amount: Float = 0
    @objc dynamic var unitIndex: Int = VolumeUnit.storedType.rawValue
    
    override var summary: String {
        let unit = VolumeUnit.init(rawValue: unitIndex)!
        let mf = MeasureFormat.volume(unit: unit)
        return String(format: "sum_meal".local,
                      mf.string(amount), DateFormat.string(from: duration))
    }
}


// MARK: - Diaper

class Diaper: Record, TypeSelectable {
    convenience init(started: Date, type: Int, color: Int, viscosity: Float, memo: String?) {
        self.init()
        self.started = started
        self.typeIndex = type
        self.colorIndex = color
        self.viscosity = viscosity
        self.memo = memo
        self.id = "DPR-\(self.id)"
    }
    
    static var typeNames: [String] = ["diaper_type0", "diaper_type1", "Wet, Solid"]
    
    @objc dynamic var typeIndex: Int = 0
    @objc dynamic var colorIndex: Int = 0
    @objc dynamic var viscosity: Float = 0.5
    
    override var summary: String { return "\(typeName)" }
}


// MARK: - Growth

class Growth: Record {
    convenience init(started: Date, height: Float, weight: Float, head: Float, memo: String?) {
        self.init()
        self.started = started
        self.height = height
        self.weight = weight
        self.head = head
        self.memo = memo
        self.id = "GRW-\(self.id)"
    }
    
    @objc dynamic var height: Float = 0
    @objc dynamic var weight: Float = 0
    @objc dynamic var head: Float = 0
    @objc dynamic var lengthUnit: Int = LengthUnit.storedType.rawValue
    @objc dynamic var weightUnit: Int = WeightUnit.storedType.rawValue
}

