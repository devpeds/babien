//
//  BaseObject.swift
//  Babien
//
//  Created by SeungHoon Jung on 2018. 3. 16..
//  Copyright © 2018년 SeungHoon Jung. All rights reserved.
//

import Foundation
import RealmSwift

// MARK: - BaseObject

class BaseObject: Object {
    
    @objc dynamic var id: String = UUID().uuidString
    
    override static func primaryKey() -> String? { return "id" }
}

// MARK: - BaseWrapper

class BaseWrapper: BaseObject {
    @objc dynamic var typeName: String = ""
    @objc dynamic var primaryKey: String = ""
}

// MARK: - Wrappable

protocol Wrappable: class {
    associatedtype W: BaseObject
    
    static var supportedClasses: [W.Type] { get }
    static var lookup: [String: W.Type] { get }
    
    var typeName: String { get set }
    var primaryKey: String { get set }
    
    var value: W { get }
}

extension Wrappable where Self: Object {
    init(_ object: W, type: W.Type) {
        self.init()
        typeName = String(describing: type)
        
        guard let primaryKeyName = type.primaryKey() else {
            fatalError("\(typeName) does not define a primary key")
        }
        
        guard let primaryKeyValue = object.value(forKey: primaryKeyName) as? String else {
            fatalError("\(typeName)'s primary key is not a `String`")
        }
        
        primaryKey = primaryKeyValue
    }
    
    internal static func createLookup() -> [String: W.Type] {
        var dict = [String: W.Type]()
        for w in Self.supportedClasses { dict[String(describing: w)] = w }
        return dict
    }
    
    var value: W {
        guard let type = Self.lookup[typeName] else {
            fatalError("unknown record type \(typeName)")
        }
        
        let realm = RealmManager.shared
        guard let value = realm.read(ofType: type, forPrimaryKey: primaryKey) else {
            fatalError("`\(typeName)` with primary key `\(primaryKey)` does not exist")
        }
        
        return value
    }
}
