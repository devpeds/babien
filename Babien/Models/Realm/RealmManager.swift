//
//  RealmManager.swift
//  Babien
//
//  Created by SeungHoon Jung on 2018. 3. 16..
//  Copyright © 2018년 SeungHoon Jung. All rights reserved.
//

import Foundation
import RealmSwift

public class RealmManager: NSObject {
    static let shared = RealmManager()
    
    private override init() { super.init() }
    
    private lazy var realm: Realm = {
        let fm = FileManager.default
        
        guard let urlGroup = fm.containerURL(forSecurityApplicationGroupIdentifier: groupId)?.appendingPathComponent(realmPath) else {
            fatalError("Cannot access app group with \(groupId). Check out app group id.")
        }
        
        guard let urlOriginal = Realm.Configuration.defaultConfiguration.fileURL else {
            return try! Realm(fileURL: urlGroup)
        }
        
        let pathGroup = urlGroup.path, pathOriginal = urlOriginal.path
        
        if fm.fileExists(atPath: pathOriginal) && !fm.fileExists(atPath: pathGroup) {
            do {
                try fm.moveItem(at: urlOriginal, to: urlGroup)
            } catch let e {
                beaver.error(e.localizedDescription)
            }
        }

        let realm = try! Realm(fileURL: urlGroup)
        beaver.debug("\(realm.configuration.fileURL!)")
        return realm
    }()
    
    
    
    // MARK: - Write
    
    func write(_ block: (() throws -> Void)) {
        do {
            try realm.write(block)
        } catch let e {
            print(e.localizedDescription)
        }
        beaver.debug("write transaction did finish")
    }
    
    // MARK: - Create
    
    func create(_ object: Object) { realm.add(object) }
    
    func createChild(name: String, gender: Int, dob: Date, due: Date, profile: UIImage?, isSelected: Bool = false) -> Child {
        let child = Child()
        write {
            child.name = name
            child.gender = gender
            child.dob = dob
            child.dueDate = due
            child.isSelected = isSelected
            if let image = profile {
                child.profile = Profile(id: child.id, image: image)
            }
            create(child)
        }
        return child
    }
    
    func create(_ record: Record, to child: Child?, stopwatch: Bool = false) -> AnyRecord {
        let anyRecord = AnyRecord(record: record)
        write {
            create(record)
            create(anyRecord)
            child?.records.append(anyRecord)
            
            if stopwatch {
                let stopwatches = createStopwatches(type: anyRecord.recordType)
                anyRecord.numberOfStopwatches = stopwatches.count
                anyRecord.stopwatches.append(objectsIn: stopwatches)
            }
        }
        return anyRecord
    }
    
    private func createStopwatches(type: RecordType) -> [Stopwatch] {
        var results: [Stopwatch] = []
        switch type {
        case .sleep: results.append(Stopwatch(max: 12*3600))
        case .breast: results.append(contentsOf: [Stopwatch(max: 3600), Stopwatch(max: 3600)])
        case .bottle: results.append(Stopwatch(max: 3600))
        case .meal: results.append(Stopwatch(max: 3600))
        case .pump: results.append(contentsOf: [Stopwatch(max: 3600), Stopwatch(max: 3600)])
        default: break
        }
        return results
    }
    
    
    // MARK: - Read
    
    func read<Element: Object>(_ type: Element.Type) -> Results<Element> { return realm.objects(type) }
    
    func read<Element: Object, KeyType>(ofType type: Element.Type, forPrimaryKey key: KeyType) -> Element? {
        return realm.object(ofType: type, forPrimaryKey: key)
    }
    
    func selectedChild() -> Results<Child> {
        return read(Child.self).filter("isSelected = %@", true)
    }
    
    // MARK: - Update
    
    func update(_ object: Object) { realm.add(object, update: true) }
    
    func update(_ child: Child, name: String, gender: Int, dob: Date, due: Date, image: UIImage?) {
        write {
            child.name = name
            child.gender = gender
            child.dob = dob
            child.dueDate = due
            
            if let profile = child.profile {
                profile.data = image?.jpeg()
                profile.image == nil ? delete(object: profile) : update(profile)
            } else {
                if let newImg = image {
                    child.profile = Profile(id: child.id, image: newImg)
                }
            }
            
            update(child)
        }
    }
    
    func update(_ record: AnyRecord, detail: (Record) -> Void) {
        write {
            let r = record.value
            detail(r)
            update(r)
            record.started = r.started
            update(record)
        }
    }
    
    func stopwatch(_ record: AnyRecord) {
        update(record) {
            let stopwatches = record.stopwatches
            stopwatches.forEach { $0.stop() }
            let ds = stopwatches.map { $0.duration }
            
            if let sleep = $0 as? Sleep {
                sleep.duration = Float(ds[0])
            }
            else if let breast = $0 as? Breastfeeding {
                breast.durationLeft = Float(ds[0])
                breast.durationRight = Float(ds[1])
            }
            else if let bottle = $0 as? Bottlefeeding {
                bottle.duration = Float(ds[0])
            }
            else if let meal = $0 as? Meal {
                meal.duration = Float(ds[0])
            }
            else if let pump = $0 as? Pump {
                pump.durationLeft = Float(ds[0])
                pump.durationRight = Float(ds[1])
            }
            
            $0.started = Date().addingTimeInterval( ds.reduce(0, { $0-$1 }) )
            record.numberOfStopwatches = 0
            delete(list: stopwatches)
            beaver.debug("number of stopwatches: \(read(Stopwatch.self).count)")
        }
    }
    
    func select(child: Child) {
        write {
            selectedChild().first?.isSelected = false
            child.isSelected = true
        }
    }
    
    
    // MARK: - Delete
    
    func delete(object: Object) { realm.delete(object) }
    
    func delete<Element: Object>(list: List<Element>) { realm.delete(list) }
    
    func delete(child: Child) {
        write {
            if let profile = child.profile { delete(object: profile) }
            delete(child.records)
            delete(object: child)
        }
    }
    
    func delete(record: AnyRecord) {
        write {
            delete(object: record.value)
            delete(list: record.stopwatches)
            delete(object: record)
        }
    }

    private func delete(_ records: List<AnyRecord>) {
        records.forEach {
            realm.delete($0.value)
            realm.delete($0.stopwatches)
            realm.delete($0)
        }
    }
    
}
