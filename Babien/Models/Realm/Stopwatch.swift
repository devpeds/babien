//
//  Stopwatch.swift
//  Babien
//
//  Created by SeungHoon Jung on 2018. 4. 5..
//  Copyright © 2018년 SeungHoon Jung. All rights reserved.
//

import RealmSwift

// MARK: - Stopwatch

class Stopwatch: BaseObject {
    convenience init(max: TimeInterval) {
        self.init()
        self.max = max
    }
    
    
    // MARK: - Stored Properties
    @objc dynamic var created: Date = Date()
    @objc dynamic var max: TimeInterval = 0
    @objc dynamic var played: Date?
    @objc dynamic var duration: TimeInterval = 0
    
    
    // MARK: - Computed Properties
    
    var isRunning: Bool { return played != nil && elapsedTime < max }
    
    var elapsedTime: TimeInterval {
        if let start = played {
            return -start.timeIntervalSinceNow + duration
        }
        return duration
    }
    
    // MARK: - Methods
    
    func start() {
        played = Date()
    }
    
    func stop() {
        duration = elapsedTime < max ? elapsedTime : max
        played = nil
    }
    
    func displayTime(showHr: Bool) -> String {
        return DateFormat.stopwatch(time: elapsedTime, showHr: showHr)
    }
}
