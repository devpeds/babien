//
//  Child.swift
//  Babien
//
//  Created by SeungHoon Jung on 2018. 3. 16..
//  Copyright © 2018년 SeungHoon Jung. All rights reserved.
//

import Foundation
import UIKitExtension
import RealmSwift

// MARK: - Child
class Child: BaseObject {
    convenience init(name: String, gender: Int, dob: Date, due: Date, profileImage: UIImage? = nil) {
        self.init()
        self.name = name
        self.gender = gender
        self.dob = dob
        self.dueDate = due
        if let image = profileImage {
            self.profile = Profile(id: self.id, image: image)
        }
    }
    
    @objc dynamic var name: String = ""
    @objc dynamic var gender: Int = 0
    @objc dynamic var dob: Date = Date()
    @objc dynamic var dueDate: Date = Date()
    @objc dynamic var isSelected: Bool = false
    
    @objc dynamic var profile: Profile?
    
    let records = List<AnyRecord>()
    
    var dobString: String { return DateFormat.string(from: dob, type: .daily) }
    
    var age: String {
        let calendar = Calendar.current
        let dc = calendar.dateComponents([.day], from: dob, to: Date())
        let age = dc.day ?? 0
        return "D+\(age)"
    }
}


// MARK: - Profile

class Profile: BaseObject {
    convenience init(id: String, image: UIImage) {
        self.init()
        self.id = id
        self.data = image.jpeg()
    }
    
    @objc dynamic var data: Data?
    
    var image: UIImage? {
        if let data = self.data { return UIImage(data: data) }
        return UIImage(named: "profile")
    }
}


