//
//  GrowthChartDataSource.swift
//  Babien
//
//  Created by SeungHoon Jung on 2018. 4. 18..
//  Copyright © 2018년 SeungHoon Jung. All rights reserved.
//

import UIKitExtension
import RealmSwift
import Charts



// MARK: - GrowthChartDataSource

class GrowthChartDataSource: StatsDataSource {
    
    // MARK: - Properties
    
    private var started: Date!
    
    private var gender: Int = 0
    
    private var type: Int = 0
    
    
    // MARK: - Data
    
    private(set) var data: ChartData!
    
    private(set) var min: Double = 0
    
    private(set) var max: Double = 0
    
    private var dict = [String:Double]()
    
    private var array = NSMutableArray()
    
    
    // MARK: - Methods
    
    func setOption(_ child: Child) {
        self.started = child.dueDate
        self.gender = child.gender
    }
    
    func setData(type: Int) {
        self.type = type
        self.array.removeAllObjects()
        createData()
    }
    
    override func createData() {
        // Growth Chart
        parse()
        var sets = createDataSets()
        // Client Data
        var clientData: LineChartDataSet, entries: DataEntries = []
        records.forEach {
            guard let g = $0.value as? Growth else { return }
            let raw = [g.height, g.weight, g.head][type]
            let unit = [g.lengthUnit, g.weightUnit, g.lengthUnit][type]
            if raw > 0 {
                let x = calendar.dateComponents([.day], from: started, to: g.started).day!
                let y = convert(Double(raw), unit: unit)
                let p = percentile(age: x, value: y)
                entries.append( ChartDataEntry(x: Double(x), y: y, data: p) )
            }
        }
        
        clientData = LineChartDataSet(values: entries, label: "Client")
        clientData.circleColors = [.main]
        clientData.circleRadius = 4.0
        clientData.lineWidth = 2.0
        clientData.colors = [.main]
        
        sets.append(clientData)
        self.data = LineChartData(dataSets: sets)
    }
    
    private func parse() {
        let pre = ["lhfa","wfa","hcfa"][type], mid = ["boys","girls"][gender]
        let resource = String(format: "%@_%@_p_exp", pre, mid)
        let path = Bundle.main.path(forResource: resource, ofType: "txt")!
        
        if FileManager.default.fileExists(atPath: path) {
            do {
                let fullTxt = try String(contentsOf: URL(fileURLWithPath: path), encoding: String.Encoding.utf8)
                let rows = fullTxt.components(separatedBy: "\r\n")
                let keys = ["Age", "L", "M", "S", "P01", "P1", "P3", "P5", "P10", "P15", "P25", "P50", "P75", "P85", "P90", "P95", "P97", "P99", "P999"]
                
                for i in 1...731 {
                    let cols = rows[i].components(separatedBy: "\t")                    
                    for j in 0..<cols.count {
                        dict.updateValue(Double(cols[j])!, forKey: keys[j])
                    }
                    
                    array.add(dict)
                }
                
                min = (array.firstObject as! [String:Double])["P01"]!
                max = (array.lastObject as! [String:Double])["P999"]!
                
            } catch let e {
                beaver.error(e.localizedDescription)
            }
        }
    }
    
    private func createDataSets() -> [LineChartDataSet] {
        var entries: [DataEntries] = [[],[],[],[],[],[],[]]
        array.forEach {
            let d = $0 as! [String: Double]
            // 1st
            entries[0].append(ChartDataEntry(x: d["Age"]!, y: d["P1"]!))
            // 10th
            entries[1].append(ChartDataEntry(x: d["Age"]!, y: d["P10"]!))
            // 25th
            entries[2].append(ChartDataEntry(x: d["Age"]!, y: d["P25"]!))
            // 50th
            entries[3].append(ChartDataEntry(x: d["Age"]!, y: d["P50"]!))
            // 75th
            entries[4].append(ChartDataEntry(x: d["Age"]!, y: d["P75"]!))
            // 90th
            entries[5].append(ChartDataEntry(x: d["Age"]!, y: d["P90"]!))
            // 99th
            entries[6].append(ChartDataEntry(x: d["Age"]!, y: d["P99"]!))
        }
        
        let sets = [LineChartDataSet(values: entries[0], label: "P1"),
                    LineChartDataSet(values: entries[1], label: "P10"),
                    LineChartDataSet(values: entries[2], label: "P25"),
                    LineChartDataSet(values: entries[3], label: "P50"),
                    LineChartDataSet(values: entries[4], label: "P75"),
                    LineChartDataSet(values: entries[5], label: "P90"),
                    LineChartDataSet(values: entries[6], label: "P99")]
        
        sets.forEach {
            $0.drawCirclesEnabled = false
            $0.colors = [.lightGray]
            $0.highlightEnabled = false
            $0.lineWidth = 1.0
        }
        
        return sets
    }
    
    private func zScore(age: Int, value: Double) -> Double {
        let d = array.object(at: age) as! [String:Double]
        let L = d["L"]!, M = d["M"]!, S = d["S"]!
        
        if L == 0 {
            return log(value/M)/S
        } else {
            return (pow(value/M, L)-1) / (L*S)
        }
    }
    
    private func percentile(age: Int, value: Double) -> NSString {
        return percentile(zScore: zScore(age: age, value: value) )
    }
    
    private func percentile(zScore z: Double) -> NSString {
        let tmp = 0.5 * (1 + erf(z / sqrt(2.0)))
        return NSString(format: "%.0f%@", tmp*100, "%")
    }
    
    private func convert(_ v: Double, unit: Int) -> Double {
        if type == 1 {
            return WeightUnit(rawValue: unit)!.convert(v, to: WeightUnit.kg)
        }
        return LengthUnit(rawValue: unit)!.convert(v, to: LengthUnit.cm)
    }
    
    
}
