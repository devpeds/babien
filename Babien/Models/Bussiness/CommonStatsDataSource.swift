//
//  CommonStatsDataSource.swift
//  Babien
//
//  Created by SeungHoon Jung on 2018. 3. 31..
//  Copyright © 2018년 SeungHoon Jung. All rights reserved.
//

import UIKitExtension
import RealmSwift
import Charts


// MARK: - StatsData

struct StatsData {
    let title: String
    let data: ChartData
    let summary: String
}


// MARK: - CommonStatsDataSource

class CommonStatsDataSource: StatsDataSource {
    
    // MARK: - Properties
    
    private var dates: Range!
    
    private(set) var data: [StatsData] = []
    
    
    // MARK: - Methods
    
    func setData(begin: Date, end: Date) {
        dates = (begin, end)
        data = []
        createData()
    }
    
    override func createData() {
        var entries = dataEntries()
        
        var d: Date = dates.begin, x: Double = 0
        while calendar.compare(d, to: dates.end, toGranularity: .day) != .orderedDescending {
            // append data entry
            let entry = dataEntry(x: x, from: d.begin, to: d.end)
            for (i, e) in entry.enumerated() {
                entries[i].append(e)
            }
            d = d.next
            x += 1
        }
        // set data set
        setChartData(entries)
    }
    
    private func dataEntries() -> [DataEntries] {
        var entries: [DataEntries] = []
        for _ in 0..<numberOfEntries() { entries.append( [ChartDataEntry]() ) }
        return entries
        
    }
    
    fileprivate func numberOfEntries() -> Int { return 0 }
    
    fileprivate func dataEntry(x: Double, from d0: Date, to d1: Date) -> DataEntries {
        return []
    }
    
    fileprivate func records(from d0: Date, to d1: Date) -> Results<AnyRecord> {
        return records.filter("started BETWEEN {%@, %@}", d0, d1)
    }
    
    fileprivate func setChartData(_ entries: [DataEntries]) {
        var titles: [String] = dataTitles()
        var summaries: [String] = dataSummaries(entries)
        
        for i in 0..<numberOfEntries() {
            let dataSet = BarChartDataSet(values: entries[i], label: nil)
            dataSet.colors = [.main]
            let chartData = BarChartData(dataSet: dataSet)
            data.append( StatsData(title: titles[i],
                                  data: chartData,
                                  summary: summaries[i]) )
        }
    }
    
    fileprivate func dataTitles() -> [String] { return [] }
    
    fileprivate func dataSummaries(_ entries: [DataEntries]) -> [String] { return [] }
    
    fileprivate func average(_ entry: DataEntries) -> Double {
        return entry.map({ $0.y }).reduce(0, { $0+$1 }) / Double( entry.count )
    }
    
    fileprivate func average(value: Double, unit: String) -> String {
        return String(format: "%.1f %@", value, unit.local)
    }
}


// MARK: - Subclasses

// MARK: - Sleep
class SleepStatsDataSource: CommonStatsDataSource {
    override func numberOfEntries() -> Int { return 1 }
    
    override func dataTitles() -> [String] { return ["Sleep Time"].local }
    
    override func dataSummaries(_ entries: [StatsDataSource.DataEntries]) -> [String] {
        let duration = average(entries[0])
        return [average(value: duration, unit: "hrs/day")]
    }
    
    override func dataEntry(x: Double, from d0: Date, to d1: Date) -> StatsDataSource.DataEntries {
        let durations = records(from: d0, to: d1).map {
            Double( ($0.value as! Sleep).duration ) / 3600
        }
        return [ BarChartDataEntry(x: x, y: durations.reduce(0, { $0+$1 })) ]
    }
}


// MARK: - Breastfeeding
class BreastStatsDataSource: CommonStatsDataSource {
    override func numberOfEntries() -> Int { return 3 }
    
    override func dataTitles() -> [String] {
        return ["Number of Feedings", "Feeding Time(L)", "Feeding Time(R)"].local
    }
    
    override func dataSummaries(_ entries: [StatsDataSource.DataEntries]) -> [String] {
        let avrgs = entries.map { average($0) }
        return [ average(value: avrgs[0], unit: "times/day"),
                 average(value: avrgs[1], unit: "hrs/day"),
                 average(value: avrgs[2], unit: "hrs/day") ]
    }
    
    override func dataEntry(x: Double, from d0: Date, to d1: Date) -> StatsDataSource.DataEntries {
        let query = records(from: d0, to: d1)
        let nf = Double( query.count )
        let dl = query.map {
            ($0.value as! Breastfeeding).durationLeft / 3600
        }
        let dr = query.map {
            ($0.value as! Breastfeeding).durationRight / 3600
        }
        return [ BarChartDataEntry(x: x, y: nf),
                 BarChartDataEntry(x: x, y: Double(dl.reduce(0, { $0+$1 }))),
                 BarChartDataEntry(x: x, y: Double(dr.reduce(0, { $0+$1 }))) ]
    }
}


// MARK: - Bottlefeeding
class BottleStatsDataSource: CommonStatsDataSource {
    override func numberOfEntries() -> Int { return 3 }
    
    override func dataTitles() -> [String] {
        return ["Number of Feedings", "Feeding Time", "Feeding Amount"].local
    }
    
    override func dataSummaries(_ entries: [StatsDataSource.DataEntries]) -> [String] {
        let avrgs = entries.map { average($0) }
        return [ average(value: avrgs[0], unit: "times/day"),
                 average(value: avrgs[1], unit: "hrs/day"),
                 average(value: avrgs[2], unit: "\(VolumeUnit.storedValue)/day") ]
    }
    
    override func dataEntry(x: Double, from d0: Date, to d1: Date) -> StatsDataSource.DataEntries {
        let query = records(from: d0, to: d1)
        let count = Double(query.count)
        let duration = query.map {
            ($0.value as! Bottlefeeding).duration / 3600
        }
        let amount = query.map { (r: AnyRecord) -> Float in
            let v = r.value as! Bottlefeeding
            return VolumeUnit.init(rawValue: v.unitIndex)!.convert(v.amount)
        }

        return [ BarChartDataEntry(x: x, y: count),
                 BarChartDataEntry(x: x, y: Double(duration.reduce(0, { $0+$1 }))),
                 BarChartDataEntry(x: x, y: Double(amount.reduce(0, { $0+$1 }))) ]
    }
}


// MARK: - Meal
class MealStatsDataSource: CommonStatsDataSource {
    override func numberOfEntries() -> Int { return 3 }
    
    override func dataTitles() -> [String] {
        return ["Number of Feedings", "Feeding Time", "Feeding Amount"].local
    }
    
    override func dataSummaries(_ entries: [StatsDataSource.DataEntries]) -> [String] {
        let avrgs = entries.map { average($0) }
        return [ average(value: avrgs[0], unit: "times/day"),
                 average(value: avrgs[1], unit: "hrs/day"),
                 average(value: avrgs[2], unit: "\(VolumeUnit.storedValue)/day") ]
    }
    
    override func dataEntry(x: Double, from d0: Date, to d1: Date) -> StatsDataSource.DataEntries {
        let query = records(from: d0, to: d1)
        let count = Double(query.count)
        let duration = query.map {
            ($0.value as! Meal).duration / 3600
        }
        let amount = query.map { (r: AnyRecord) -> Float in
            let v = r.value as! Meal
            return VolumeUnit.init(rawValue: v.unitIndex)!.convert(v.amount)
        }
        return [ BarChartDataEntry(x: x, y: count),
                 BarChartDataEntry(x: x, y: Double(duration.reduce(0, { $0+$1 }))),
                 BarChartDataEntry(x: x, y: Double(amount.reduce(0, { $0+$1 }))) ]
    }
}


// MARK: - Pump
class PumpStatsDataSource: CommonStatsDataSource {
    override func numberOfEntries() -> Int { return 4 }
    
    override func dataTitles() -> [String] {
        return ["Number of Pumps", "Pump Time(L)", "Pump Time(R)", "Pump Amount"].local
    }
    
    override func dataSummaries(_ entries: [StatsDataSource.DataEntries]) -> [String] {
        let avrgs = entries.map { average($0) }
        return [ average(value: avrgs[0], unit: "times/day"),
                 average(value: avrgs[1], unit: "hrs/day"),
                 average(value: avrgs[2], unit: "hrs/day"),
                 average(value: avrgs[3], unit: "\(VolumeUnit.storedValue)/day") ]
    }
    
    override func dataEntry(x: Double, from d0: Date, to d1: Date) -> StatsDataSource.DataEntries {
        let query = records(from: d0, to: d1)
        let pc = Double( query.count )
        let pl = query.map { ($0.value as! Pump).durationLeft / 3600 }
        let pr = query.map { ($0.value as! Pump).durationRight / 3600 }
        let pa = query.map { (r: AnyRecord) -> Float in
            let v = r.value as! Pump
            return VolumeUnit.init(rawValue: v.unitIndex)!.convert(v.amount)
        }
        return [ BarChartDataEntry(x: x, y: pc),
                 BarChartDataEntry(x: x, y: Double(pl.reduce(0, { $0+$1 }))),
                 BarChartDataEntry(x: x, y: Double(pr.reduce(0, { $0+$1 }))),
                 BarChartDataEntry(x: x, y: Double(pa.reduce(0, { $0+$1 }))) ]
    }
}


// MARK: - Diaper
class DiaperStatsDataSource: CommonStatsDataSource {
    override func numberOfEntries() -> Int { return 3 }
    
    override func dataTitles() -> [String] {
        return ["Number of Diapers(Total)",
                "Number of Diapers(Wet)",
                "Number of Diapers(Solid)"].local
    }
    
    override func dataSummaries(_ entries: [StatsDataSource.DataEntries]) -> [String] {
        let avrgs = entries.map { average($0) }
        return [ average(value: avrgs[0], unit: "times/day"),
                 average(value: avrgs[1], unit: "times/day"),
                 average(value: avrgs[2], unit: "times/day"), ]
    }
    
    override func dataEntry(x: Double, from d0: Date, to d1: Date) -> StatsDataSource.DataEntries {
        let query = records(from: d0, to: d1)
        let predicate: (AnyRecord, Int) -> Bool = {
            let d = ($0.value as! Diaper)
            return d.typeIndex == $1 || d.typeIndex == 2
        }
        let total = Double( query.count )
        let wet = Double( query.filter({ predicate($0, 0) }).count )
        let solid = Double( query.filter({ predicate($0, 1) }).count )
        return [ BarChartDataEntry(x: x, y: total),
                 BarChartDataEntry(x: x, y: wet),
                 BarChartDataEntry(x: x, y: solid) ]
    }
}
