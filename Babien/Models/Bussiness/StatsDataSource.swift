//
//  StatsDataSource.swift
//  Babien
//
//  Created by SeungHoon Jung on 2018. 4. 18..
//  Copyright © 2018년 SeungHoon Jung. All rights reserved.
//

import UIKitExtension
import RealmSwift
import Charts


// MARK: - StatsDataSource(Superclass)

class StatsDataSource: NSObject {
    
    // MARK: - Typealias
    typealias Range = (begin: Date, end: Date)
    typealias DataEntries = [ChartDataEntry]
    
    
    // MARK: - Initializers
    
    init(records: Results<AnyRecord>) {
        super.init()
        self.records = records
    }
    
    
    // MARK: - Properties
    
    internal var calendar: Calendar { return Calendar.current }
    
    internal var records: Results<AnyRecord>!
    
    
    // MARK: - Methods
    
    internal func createData() {}
    
}
