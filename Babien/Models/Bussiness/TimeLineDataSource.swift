//
//  TimeLineDataSource.swift
//  Babien
//
//  Created by SeungHoon Jung on 2018. 4. 6..
//  Copyright © 2018년 SeungHoon Jung. All rights reserved.
//

import Foundation
import RealmSwift

// MARK: - Model

class TimeLineDataSource: NSObject {
    
    deinit { beaver.debug("TimeLineDataSource: deinit")}
    
    init(records: Results<AnyRecord>?) {
        super.init()
        beaver.debug("TimeLineDataSource: init")
        self.setup(records)
    }
    
    private var calendar: Calendar { return Calendar.current }
    
    private(set) var sections: [(date: Date?, count: Int)] = []
    private(set) var items: [Results<AnyRecord>] = []
    
    
    private func setup(_ record: Results<AnyRecord>?) {
        guard let r = record else {
            return beaver.debug("TimeLineDataSource: the result is nil")
        }
        
        beaver.debug("TimeLineDataSource: the number of results: \(r.count)")
        var i = 0
        // section 0 : stopwatches
        let stopwatches = r.filter("numberOfStopwatches > 0")
        sections.append((nil, stopwatches.count))
        items.append(stopwatches)
        beaver.debug("section 0: \(stopwatches.count) stopwatche(s)")
        i += 1
        
        // section 1~ : records
        let records = r.filter("numberOfStopwatches = 0")
        if let start = records.first?.started, let end = records.last?.started {
            var d = start
            while calendar.compare(d, to: end, toGranularity: .day) != .orderedAscending {
                let result = records.filter("started BETWEEN {%@, %@}", d.begin, d.end)
                if result.count != 0 {
                    sections.insert((d, result.count), at: i)
                    items.insert(result, at: i)
                    beaver.debug("section \(i): \(sections[i])")
                    i += 1
                }
                d = d.previous
            }
        }
    }
    
    func reload(_ records: Results<AnyRecord>?) {
        beaver.debug("data reload")
        sections = []
        items = []
        setup(records)
    }
    
    func checkStopwatch(_ type: RecordType) -> Results<AnyRecord> {
        var sw = items[0]
        switch type {
        case .breast: break
        case .pump: sw = sw.filter("typeName != %@", "Sleep")
        default: sw = sw.filter("typeName != %@", "Pump")
        }
        return sw
    }
}
