//
//  SettingsViewController.swift
//  Babien
//
//  Created by SeungHoon Jung on 2018. 3. 18..
//  Copyright © 2018년 SeungHoon Jung. All rights reserved.
//

import UIKitExtension

// MARK: - Models

class SettingDataSource: NSObject {
    let data: [SettingGroup] = [
        SettingGroup(title: "settings_unit".local,
                     settings: [Setting(title: "unit_volume".local, option: VolumeUnit.self),
                                Setting(title: "unit_length".local, option: LengthUnit.self),
                                Setting(title: "unit_weight".local, option: WeightUnit.self)])
    ]
}

struct SettingGroup {
    let title: String
    let settings: [Setting]
}

struct Setting {
    init<T: KeyValueSettable>(title: String, option: T.Type) {
        self.title = title
        self.keyName = option.keyName
        self.optionList = option.nameList
    }
    
    let title: String
    let keyName: String
    let optionList: [String]
}


// MARK: - Views

class SettingCell: TableCell {
    var setting: Setting!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var optionSwitch: SegmentedControlBubble!
    
    @objc func setOption(_ sender: SegmentedControl) {
        UserDefaults.shared.set(sender.selectedItem, forKey: setting.keyName)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        titleLabel.text = setting.title
        optionSwitch.font = UIFont.boldSystemFont(ofSize: 18)
        optionSwitch.setItems(titles: setting.optionList)
        optionSwitch.selectItem(UserDefaults.standard.integer(forKey: setting.keyName))
        optionSwitch.addTarget(self, action: #selector(setOption), for: .valueChanged)
    }
}


// MARK: - Controller

class SettingsViewController: TableViewController {
    
    private let source = SettingDataSource().data
    
    
    // MARK: - Setup
    
    override func setupAfterLoad() {
        let item = UIBarButtonItem(image: #imageLiteral(resourceName: "cancel"), style: .done, target: self, action: #selector(cancel))
        navigationItem.rightBarButtonItem = item
        
        title = "settings_title".local
    }
    
    override func setupBeforeDisappear() {
        UserDefaults.synchronize()
    }
    
    
    // MARK: - IBActions && Selectors
    
    @objc func cancel() {
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: - DataSource && Delegate
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return source.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return source[section].settings.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: SettingCell = tableView.dequeueCell()
        let group = source[indexPath.section]
        cell.setting = group.settings[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return source[section].title
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        header.textLabel?.textColor = .main
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 80.0
    }
    
}
