//
//  RootViewController.swift
//  Babien
//
//  Created by SeungHoon Jung on 2018. 4. 2..
//  Copyright © 2018년 SeungHoon Jung. All rights reserved.
//

import UIKitExtension
import GoogleMobileAds

class MainNavigationController: UINavigationController, GADInterstitialDelegate {
    
    private lazy var homeViewController: HomeViewController = {
        return UIStoryboard.viewController(from: .main)
    }()
    
    private var interstitial: GADInterstitial!
    
    private lazy var bannerView: GADBannerView = {
        let view = GADBannerView(adSize: kGADAdSizeBanner)
        view.adUnitID = "ca-app-pub-3182275476710852/7127515636" // real
//        view.adUnitID = "ca-app-pub-3940256099942544/2934735716" // test
        view.rootViewController = self
        return view
    }()
    
    let testDevices: [Any] = [kGADSimulatorID, "426210b8bee3b6515173b010e71a6fb1"]
    
    
    // MARK: - LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        beaver.verbose("MainNavigationController did load")
        let request = GADRequest()
        request.testDevices = testDevices
        bannerView.load(request)
        addBanner()
        interstitial = createInterstitial()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        beaver.verbose("MainNavigationController will appear")
        view.backgroundColor = .white
        customize()
        let realm = RealmManager.shared
        if realm.selectedChild().count != 0 {
            viewControllers = [homeViewController]
            if interstitial.isReady {
                beaver.debug("good to go")
                interstitial.present(fromRootViewController: self)
            } else {
                beaver.debug("fail to present")
            }
        } else {
            perform(#selector(callChildForm), with: nil, afterDelay: 0.0)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        beaver.verbose("MainNavigationController did appear")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        beaver.verbose("MainNavigationController will disappear")
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        beaver.verbose("MainNavigationController did disappear")
    }
    
    
    // MARK: - Methods
    
    func addBanner() {
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(bannerView)
        bannerView.anchorCenterXToSuperview()
        bannerView.anchor(bottom: view.safeAreaLayoutGuide.bottomAnchor, 0)
    }
    
    func createInterstitial() -> GADInterstitial {
        let ad = GADInterstitial(adUnitID: "ca-app-pub-3182275476710852/7287297731") // real
//        let ad = GADInterstitial(adUnitID: "ca-app-pub-3940256099942544/4411468910") // test
        ad.delegate = self
        let request = GADRequest()
        request.testDevices = testDevices
        ad.load(request)
        return ad
    }
    
    @objc func callChildForm() {
        let vc: UIViewController
        if RealmManager.shared.read(Child.self).count == 0 {
            let form: ChildFormViewController = UIStoryboard.viewController(from: .child)
            form.isFirstChild = true
            vc = form
        } else {
            let list: ChildListViewController = UIStoryboard.viewController(from: .child)
            vc = list
        }
        present(UINavigationController(rootViewController: vc), animated: true, completion: nil)
    }
    
    
    // MARK: - GADInterstitialDelegate
    
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        interstitial = createInterstitial()
    }
}
