//
//  TrackerViewController.swift
//  Babien
//
//  Created by SeungHoon Jung on 2018. 3. 30..
//  Copyright © 2018년 SeungHoon Jung. All rights reserved.
//

import UIKitExtension

// MARK: - TrackerForm

extension TrackerForm: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return titles.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return inputs[section].count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: TrackerCell = collectionView.dequeueCell(for: indexPath)
        cell.form = inputs[indexPath.section][indexPath.row]
        cell.shadow(offset: (0,0), radius: 1, opacity: 0.8, color: .lightGray)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header: TrackerHeader = collectionView.dequeueHeader(for: indexPath)
        header.title = titles[indexPath.section]
        header.shadow(offset: (0,0), radius: 1, opacity: 0.8, color: .lightGray)
        return header
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let input = inputs[indexPath.section][indexPath.item]
        return CGSize(width: collectionView.frame.width * (input.multiplier), height: input.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 30)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 30, right: 0)
    }
}

extension RecordType {
    func trackerForm(_ cv: UICollectionView, record: AnyRecord) -> TrackerForm {
        switch self {
        case .sleep: return SleepForm(cv, record: record)
        case .breast: return BreastForm(cv, record: record)
        case .bottle: return BottleForm(cv, record: record)
        case .meal: return MealForm(cv, record: record)
        case .pump: return PumpForm(cv, record: record)
        case .diaper: return DiaperForm(cv, record: record)
        case .growth: return GrowthForm(cv, record: record)
        }
    }
}


// MARK: - Controllers

class TrackerViewController: ViewController {
    
    // MARK: - IBOutlets && UI Components
    
    @IBOutlet weak var formView: UICollectionView!
    
    lazy private var saveItem: UIBarButtonItem = {
        let item = UIBarButtonItem(image: #imageLiteral(resourceName: "check"), style: .done, target: self, action: #selector(save))
        item.tintColor = .main
        return item
    }()
    
    
    // MARK: - Properties
    
    var child: Child!
    var record: AnyRecord!
    var type: RecordType!
    var form: TrackerForm!
    
    
    // MARK: - Setup
    
    override func setupAfterLoad() {
        title = type.title.local
        navigationItem.rightBarButtonItem = saveItem
        form = type.trackerForm(formView, record: record)
    }
    
    override func setupBeforAppear() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func setupBeforeDisappear() {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    
    // MARK: - IBActions && Selectors
    
    @objc func save() {
        form.save(for: child)
        navigationController?.popViewController(animated: true)
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        if let info = notification.userInfo {
            let keyboardInfo = info[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue
            let height = keyboardInfo.cgRectValue.height
            let insets = UIEdgeInsets(top: 0, left: 0, bottom: height, right: 0)
            formView.contentInset = insets
            formView.scrollIndicatorInsets = insets
        }
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        formView.contentInset = .zero
        formView.scrollIndicatorInsets = .zero
    }
    
}
