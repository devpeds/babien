//
//  TrackerInput.swift
//  Babien
//
//  Created by SeungHoon Jung on 2018. 3. 31..
//  Copyright © 2018년 SeungHoon Jung. All rights reserved.
//

import UIKitExtension

// MARK: - Protocol && Superclass

protocol TrackerInputSupport {
    associatedtype ValueType
    associatedtype InputView: UIView
    associatedtype SupplementaryView: UIView
    
    var inputView: InputView! { get }
    var supplementaryView: SupplementaryView! { get }
    var value: ValueType { get }
}

class TrackerInput: NSObject {
    
    init(height: CGFloat, numberOfItems: Int = 1) {
        self.height = height
        self.multiplier = 1 / CGFloat(numberOfItems)
    }
    
    private(set) var height: CGFloat
    private(set) var multiplier: CGFloat
    
    func setNumberOfItems(_ count: Int) { self.multiplier = 1 / CGFloat(count) }
    
    func setup(_ superView: UIView) {}
    
}


// MARK: - SubClasses

// MARK: - TimeInput
class TimeInput: TrackerInput, TrackerInputSupport, DateFormTextFieldDelegate {
    init(date: Date, delegate: UITextFieldDelegate) {
        self.inputView = DateFormTextField(date: date)
        self.supplementaryView = UILabel()
        super.init(height: 125.0)
        
        inputView.font = UIFont.boldSystemFont(ofSize: 32)
        inputView.textColor = .darkGray
        inputView.dateDelegate = self
        inputView.delegate = delegate
        inputView.displayText()
        
        supplementaryView.font = UIFont.systemFont(ofSize: 16)
        supplementaryView.textColor = .darkGray
    }
    
    var inputView: DateFormTextField!
    var supplementaryView: UILabel!
    var value: Date { return inputView.date }
    
    override func setup(_ superView: UIView) {
        super.setup(superView)
        superView.addSubview(inputView)
        inputView.anchorCenterToSuperview()
        
        superView.addSubview(supplementaryView)
        supplementaryView.anchorCenterToSuperview(offset: 0, -28)
    }
    
    func setDatePicker(_ datePicker: UIDatePicker) {
        datePicker.datePickerMode = .dateAndTime
        datePicker.maximumDate = Date()
    }
    
    func displayText(textField: FormTextField, datePicker: UIDatePicker) -> String {
        supplementaryView.text = DateFormat.string(from: datePicker.date, type: .daily)
        return DateFormat.string(from: datePicker.date, type: .time)
    }
}


// MARK: - DurationInput
class DurationInput: TrackerInput, TrackerInputSupport {
    init(current: Float, max: Float, numberOfItems: Int = 1) {
        self.inputView = UISlider()
        self.supplementaryView = UILabel()
        super.init(height: 100, numberOfItems: numberOfItems)
        
        supplementaryView.font = UIFont.boldSystemFont(ofSize: 24)
        supplementaryView.textColor = .darkGray
        supplementaryView.text = DateFormat.string(from: current)
        
        inputView.tintColor = UIColor.main
        inputView.minimumValue = 0
        inputView.maximumValue = max
        inputView.value = current
        inputView.addTarget(self, action: #selector(valueDidChanged), for: .valueChanged)
    }
    
    var inputView: UISlider!
    
    var supplementaryView: UILabel!
    
    var value: Float {
        let step: Float = 300 // 5 min
        return floorf(inputView.value / step) * step
    }
    
    override func setup(_ superView: UIView) {
        super.setup(superView)
        
        superView.addSubview(inputView)
        inputView.anchorCenterToSuperview(offset: 0, 24)
        inputView.anchor(left: superView.leftAnchor, 24)
        
        superView.addSubview(supplementaryView)
        supplementaryView.anchorCenterToSuperview(offset: 0, -18)
    }
    
    @objc func valueDidChanged() {
        let step: Float = 300 // 5 min
        let newVal = floorf(inputView.value / step) * step
        supplementaryView.text = DateFormat.string(from: newVal)
    }
}


// MARK: - AmountInput
class AmountInput: TrackerInput, TrackerInputSupport {
    
    init(amount: (Float, VolumeUnit), max: (Float, VolumeUnit), delegate: UITextFieldDelegate) {
        let a = (amount.1).convert(amount.0)
        let m = (max.1).convert(max.0)
        let unit = VolumeUnit.storedType
        let isml = unit == .ml
        let step: Float = isml ? 1.0 : 0.5
        self.inputView = MeasureFormTextField(value: a, isInteger: isml, max: m, unit: unit.name)
        self.supplementaryView = UISlider()
        self.step = step
        super.init(height: 125)
        
        inputView.frame = CGRect(x: 0, y: 0, width: 0, height: 30)
        inputView.font = UIFont.boldSystemFont(ofSize: 24)
        inputView.textAlignment = .right
        inputView.textColor = .darkGray
        inputView.delegate = delegate
        inputView.addTarget(self, action: #selector(textFieldDidChanged(_:)), for: .valueChanged)
        
        supplementaryView.tintColor = UIColor.main
        supplementaryView.minimumValue = 0
        supplementaryView.maximumValue = m
        supplementaryView.value = a
        supplementaryView.addTarget(self, action: #selector(valueDidChanged), for: .valueChanged)
    }
    
    var inputView: MeasureFormTextField!
    
    var supplementaryView: UISlider!
    
    var step: Float
    
    var value: Float { return inputView.value }
    
    override func setup(_ superView: UIView) {
        super.setup(superView)
        superView.addSubview(supplementaryView)
        supplementaryView.anchorCenterToSuperview(offset: 0, 24)
        supplementaryView.anchor(left: superView.leftAnchor, 24)
        
        superView.addSubview(inputView)
        inputView.anchorCenterToSuperview(offset: 0, -18)
        inputView.widthAnchor(120)
    }
    
    @objc func textFieldDidChanged(_ sender: MeasureFormTextField) {
        supplementaryView.value = sender.value
    }
    
    @objc func valueDidChanged() {
        let newVal = floorf(supplementaryView.value / step) * step
        let format = step >= 1 ? "%0.0f" : "%0.1f"
        inputView.text = String(format: format, newVal)
        if newVal != value { inputView.update(value: newVal) }
    }
}


// MARK: - MeasureInput
class MeasureInput: TrackerInput, TrackerInputSupport {
    init<T: MetricConvertable>(name: String, value: (Float, T), max: (Float, T), step: Float, unit: T, delegate: UITextFieldDelegate) {
        let v = (value.1).convert(value.0)
        let m = (max.1).convert(max.0)
        
        self.step = step
        self.inputView = MeasureFormTextField(value: v, isInteger: false, max: m, unit: unit.name, placeholder: name.local)
        super.init(height: 75)
        
        inputView.frame = CGRect(x: 0, y: 0, width: 0, height: 45)
        inputView.font = UIFont.boldSystemFont(ofSize: 24)
        inputView.textColor = .darkGray
        inputView.textAlignment = .right
        inputView.tintColor = .darkGray
        inputView.delegate = delegate
        inputView.leftImage = UIImage(named: name)
    }
    
    var inputView: MeasureFormTextField!
    
    var supplementaryView: UIView!
    
    var step: Float
    
    var value: Float { return inputView.value }
    
    
    override func setup(_ superView: UIView) {
        super.setup(superView)
        superView.addSubview(inputView)
        inputView.anchorCenterToSuperview()
        inputView.anchor(left: superView.leftAnchor, 24, height: 45)
    }
}


// MARK: - TypeInput
class TypeInput: TrackerInput, TrackerInputSupport {
    init(options: [String], selected: Int = 0, action: ((Radio) -> Void)? = nil) {
        self.inputView = Radio(titles: options)
        self.action = action
        super.init(height: 100)
        
        inputView.selectedItem = selected
        inputView.addTarget(self, action: #selector(buttonDidTapped(_:)), for: .valueChanged)
    }
    
    var inputView: Radio!
    
    var supplementaryView: UIView!
    
    var action: ((Radio) -> Void)?
    
    var value: Int { return inputView.selectedItem }
    
    override func setup(_ superView: UIView) {
        super.setup(superView)
        superView.addSubview(inputView)
        inputView.anchorCenterToSuperview()
        inputView.anchor(top: superView.topAnchor, 24, left: superView.leftAnchor, 24)
        inputView.cornerRadius = 26
    }
    
    @objc func buttonDidTapped(_ sender: Radio) { action?(sender) }
}


// MARK: - ColorInput
class ColorInput: TrackerInput, TrackerInputSupport {
    init(selected: Int = 0, action: ((ColorSelector) -> Void)? = nil) {
        self.inputView = ColorSelector()
        self.action = action
        super.init(height: 75)
        
        inputView.selectedItem = selected
        inputView.addTarget(self, action: #selector(buttonDidTapped(_:)), for: .valueChanged)
    }
    
    var inputView: ColorSelector!
    
    var supplementaryView: UIView!
    
    var value: Int { return inputView.selectedItem }
    
    var action: ((ColorSelector) -> Void)?
    
    
    override func setup(_ superView: UIView) {
        super.setup(superView)
        superView.addSubview(inputView)
        inputView.anchorCenterToSuperview()
        inputView.anchor(top: superView.topAnchor, 24, left: superView.leftAnchor, 24)
        inputView.cornerRadius = 13
        
    }
    
    @objc func buttonDidTapped(_ sender: ColorSelector) { action?(sender) }
}


// MARK: - ViscosityInput
class ViscosityInput: TrackerInput, TrackerInputSupport {
    init(viscosity: Float, color: UIColor) {
        self.inputView = UISlider()
        self.supplementaryView = UILabel()
        super.init(height: 125)
        
        inputView.minimumValue = 0
        inputView.maximumValue = 1
        inputView.value = viscosity
        inputView.tintColor = color
        inputView.addTarget(self, action: #selector(valueDidChanged), for: .valueChanged)
        
        supplementaryView.font = UIFont.boldSystemFont(ofSize: 27)
        supplementaryView.textColor = .darkGray
    }
    
    var inputView: UISlider!
    
    var supplementaryView: UILabel!
    
    var value: Float { return inputView.value }
    
    let displayTexts: [String] = "diaper_solid".list(count: 7).local
    
    override func setup(_ superView: UIView) {
        super.setup(superView)
        valueDidChanged()
        
        superView.addSubview(inputView)
        inputView.anchorCenterToSuperview(offset: 0, 24)
        inputView.anchor(left: superView.leftAnchor, 24)
        
        superView.addSubview(supplementaryView)
        supplementaryView.anchorCenterToSuperview(offset: 0, -18)
        
        let label = UILabel(text: "tracker_solid_label".local,
                            color: .lightGray,
                            font: UIFont.boldSystemFont(ofSize: 16))
        superView.addSubview(label)
        label.anchor(top: superView.topAnchor, 16, left: superView.leftAnchor, 24)
    }
    
    @objc func valueDidChanged() {
        let step = Float(displayTexts.count)
        let index = Int(floor(inputView.value * step))
        supplementaryView.text = displayTexts[(index < displayTexts.count ? index : index-1)]
    }
}


// MARK: - MemoForm

class MemoInput: TrackerInput, TrackerInputSupport {
    
    init(text: String? = nil, delegate: UITextViewDelegate) {
        self.inputView = UITextView()
        super.init(height: 250)
        
        inputView.delegate = delegate
        inputView.autocorrectionType = .no
        inputView.textColor = .darkGray
        inputView.font = UIFont.boldSystemFont(ofSize: 18)
        if let text = text { inputView.text = text }
    }
    
    var inputView: UITextView!
    
    var supplementaryView: UIView!
    
    var value: String { return inputView.text }
    
    override func setup(_ superView: UIView) {
        super.setup(superView)
        superView.addSubview(inputView)
        inputView.fillSuperview(margin: 24, 0)
    }
    
}
