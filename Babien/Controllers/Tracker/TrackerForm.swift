//
//  TrackerForm.swift
//  Babien
//
//  Created by SeungHoon Jung on 2018. 3. 31..
//  Copyright © 2018년 SeungHoon Jung. All rights reserved.
//

import UIKitExtension

// MARK: - Protocols

protocol FormUpdateSupport: class {
    func updateView(_ sender: Radio)
    func batchUpdates(_ item: Int)
}

extension FormUpdateSupport where Self: TrackerForm {
    func updateView(_ sender: Radio) {
        formView.performBatchUpdates({
            batchUpdates(sender.selectedItem)
        }, completion: nil)
    }
}

// MARK: - SuperClass

class TrackerForm: NSObject, UITextFieldDelegate, UITextViewDelegate {
    
    // MARK: - Initializers
    
    init(_ cv: UICollectionView, record: AnyRecord) {
        super.init()
        self.formView = cv
        self.formView.dataSource = self
        self.formView.delegate = self
        self.formView.registerHeader(TrackerHeader.self)
        self.formView.registerCell(TrackerCell.self)
        self.record = record
        setup()
    }
    
    
    // MARK: - Properties
    
    weak var formView: UICollectionView!
    weak var record: AnyRecord!
    
    internal let realm = RealmManager.shared
    
    var titles: [String] = ["tracker_time", "tracker_detail", "tracker_memo"].local
    var inputs: [[TrackerInput]] = []
    
    var formInputs: [FormInput] = []
    
    lazy var toolbar: FormToolbar = {
        let tb = FormToolbar(inputs: self.formInputs)
        tb.setButtonsTintColor(.darkGray)
        return tb
    }()
    
    // MARK: - Methods
    
    func setup() {}
    
    func save(for child: Child) {}
    
    func value<T: TrackerInput, S>(from input: T) -> S where T: TrackerInputSupport, T.ValueType == S {
        return input.value
    }
    
    func inputView<T: TrackerInput>(of input: T) -> UIView where T: TrackerInputSupport {
        return input.inputView
    }
    
    func setFormInputs<T: TrackerInput>(inputs: [T]) where T: TrackerInputSupport {
        inputs.forEach {
            if let textField = self.inputView(of: $0) as? UITextField {
                self.formInputs.append(textField)
            } else if let textView = self.inputView(of: $0) as? UITextView {
                self.formInputs.append(textView)
            }
        }
    }
    
    // MARK: - TextFieldDelegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        toolbar.update()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let measureField = textField as? MeasureFormTextField {
            return measureField.convertToNumber(range: range, replacement: string)
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        toolbar.goForward()
        return true
    }
    
    // MARK: - TextViewDelegate
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        beaver.debug(toolbar.currentInput === textView)
        toolbar.update()
    }
    
}


// MARK: - Subclasses

// MARK: - SleepForm
class SleepForm: TrackerForm {
    
    // MARK: - Properties
    
    private var timeInput: TimeInput!
    private var durationInput: DurationInput!
    private var typeInput: TypeInput!
    private var memoInput: MemoInput!
    
    
    // MARK: - Methods
    
    override func setup() {
        if let sleep = record.value as? Sleep {
            let t = sleep.started, d = sleep.duration, s = sleep.typeIndex, m = sleep.memo
            timeInput = TimeInput(date: t, delegate: self)
            durationInput = DurationInput(current: d, max: 12*3600)
            typeInput = TypeInput(options: "sleep_tracker".list(count: 2).local, selected: s)
            memoInput = MemoInput(text: m, delegate: self)
            
            inputs = [ [timeInput, durationInput], [typeInput], [memoInput] ]
            formInputs = [timeInput.inputView, memoInput.inputView]
        }
    }
    
    override func save(for child: Child) {
        let t = timeInput.value, d = durationInput.value, s = typeInput.value, m = memoInput.value
        realm.update(record) {
            if let sleep = $0 as? Sleep {
                sleep.started = t
                sleep.duration = d
                sleep.typeIndex = s
                sleep.memo = m
            }
        }
    }
}

// MARK: - BreastForm
class BreastForm: TrackerForm {
    
    // MARK: - Properties
    
    private var timeInput: TimeInput!
    private var durationInputs: [DurationInput] = []
    private var memoInput: MemoInput!
    
    
    // MARK: - Methods
    
    override func setup() {
        if let breast = record.value as? Breastfeeding {
            let t = breast.started, d1 = breast.durationLeft, d2 = breast.durationRight, m = breast.memo
            
            timeInput = TimeInput(date: t, delegate: self)
            durationInputs = [
                DurationInput(current: d1, max: 3600, numberOfItems: 2),
                DurationInput(current: d2, max: 3600, numberOfItems: 2)
            ]
            memoInput = MemoInput(text: m, delegate: self)
            
            titles = ["tracker_time", "tracker_memo"].local
            inputs = [ [timeInput, durationInputs[0], durationInputs[1]], [memoInput]]
            formInputs = [timeInput.inputView, memoInput.inputView]
        }
    }
    
    override func save(for child: Child) {
        let t = timeInput.value, d = durationInputs.map { $0.value }, m = memoInput.value
        realm.update(record) {
            if let breast = $0 as? Breastfeeding {
                breast.started = t
                breast.durationLeft = d[0]
                breast.durationRight = d[1]
                breast.memo = m
            }
        }
    }
}

// MARK: - BottleForm
class BottleForm: TrackerForm {
    
    // MARK: - Properties
    
    private var timeInput: TimeInput!
    private var durationInput: DurationInput!
    private var typeInput: TypeInput!
    private var amountInput: AmountInput!
    private var memoInput: MemoInput!
    
    
    // MARK: - Methods
    
    override func setup() {
        if let bottle = record.value as? Bottlefeeding {
            let t = bottle.started, d = bottle.duration, s = bottle.typeIndex, a = bottle.amount, u = VolumeUnit.init(rawValue: bottle.unitIndex)!, m = bottle.memo
            
            timeInput = TimeInput(date: t, delegate: self)
            durationInput = DurationInput(current: d, max: 3600)
            typeInput = TypeInput(options: "bottle_type".list(count: 3).local, selected: s)
            amountInput = AmountInput(amount: (a,u), max: (500, .ml), delegate: self)
            memoInput = MemoInput(text: m, delegate: self)
            
            inputs = [ [timeInput, durationInput], [typeInput, amountInput], [memoInput] ]
            formInputs = [timeInput.inputView, amountInput.inputView, memoInput.inputView]
            
        }
    }
    
    override func save(for child: Child) {
        let t = timeInput.value, d = durationInput.value, s = typeInput.value, a = amountInput.value, m = memoInput.value
        realm.update(record) {
            if let bottle = $0 as? Bottlefeeding {
                bottle.started = t
                bottle.duration = d
                bottle.typeIndex = s
                bottle.amount = a
                bottle.unitIndex = VolumeUnit.storedType.rawValue
                bottle.memo = m
            }
        }
    }
}

// MARK: - MealForm
class MealForm: TrackerForm {
    
    // MARK: - Properties
    
    private var timeInput: TimeInput!
    private var durationInput: DurationInput!
    private var amountInput: AmountInput!
    private var memoInput: MemoInput!
    
    
    // MARK: - Methods
    
    override func setup() {
        if let meal = record.value as? Meal {
            let t = meal.started, d = meal.duration, a = meal.amount, u = VolumeUnit.init(rawValue: meal.unitIndex)!, m = meal.memo
            
            timeInput = TimeInput(date: t, delegate: self)
            durationInput = DurationInput(current: d, max: 3600)
            amountInput = AmountInput(amount: (a, u), max: (500, .ml), delegate: self)
            memoInput = MemoInput(text: m, delegate: self)
            
            inputs = [ [timeInput, durationInput], [amountInput], [memoInput] ]
            formInputs = [timeInput.inputView, amountInput.inputView, memoInput.inputView]
        }
    }
    
    override func save(for child: Child) {
        let t = timeInput.value, d = durationInput.value, a = amountInput.value, m = memoInput.value
        realm.update(record) {
            if let meal = $0 as? Meal {
                meal.started = t
                meal.duration = d
                meal.amount = a
                meal.unitIndex = VolumeUnit.storedType.rawValue
                meal.memo = m
            }
        }
    }
}

// MARK: - PumpForm
class PumpForm: TrackerForm, FormUpdateSupport {
    
    // MARK: - Properties
    
    private var timeInput: TimeInput!
    private var durationInputs: [DurationInput] = []
    private var typeInput: TypeInput!
    private var amountInput: AmountInput!
    private var memoInput: MemoInput!
    
    
    // MARK: - Methods
    
    override func setup() {
        if let pump = record.value as? Pump {
            let t = pump.started, d1 = pump.durationLeft, d2 = pump.durationRight, s = pump.typeIndex, a = pump.amount, u = VolumeUnit.init(rawValue: pump.unitIndex)!, m = pump.memo
            
            timeInput = TimeInput(date: t, delegate: self)
            durationInputs = [ DurationInput(current: d1, max: 3600, numberOfItems: 1),
                               DurationInput(current: d2, max: 3600, numberOfItems: 1) ]
            typeInput = TypeInput(options: "pump_type".list(count: 3).local, selected: s) {
                [unowned self] in self.updateView($0)
            }
            amountInput = AmountInput(amount: (a, u), max: (500, .ml), delegate: self)
            memoInput = MemoInput(text: m, delegate: self)
            
            if s == 0 {
                inputs = [ [timeInput, durationInputs[0]], [typeInput, amountInput], [memoInput] ]
            } else if s == 1 {
                inputs = [ [timeInput, durationInputs[1]], [typeInput, amountInput], [memoInput] ]
            } else {
                durationInputs.forEach { $0.setNumberOfItems(2) }
                inputs = [ [timeInput, durationInputs[0], durationInputs[1]], [typeInput, amountInput], [memoInput] ]
            }
            
            formInputs = [timeInput.inputView, amountInput.inputView, memoInput.inputView]
        }
    }
    
    override func save(for child: Child) {
        let t = timeInput.value, d = durationInputs.map { $0.value }, s = typeInput.value, a = amountInput.value, m = memoInput.value
        realm.update(record) {
            if let pump = $0 as? Pump {
                pump.started = t
                pump.durationLeft = d[0]
                pump.durationRight = d[1]
                pump.typeIndex = s
                pump.amount = a
                pump.unitIndex = VolumeUnit.storedType.rawValue
                pump.memo = m
            }
        }
    }
    
    func batchUpdates(_ item: Int) {
        if item == 0 {
            let needsDeletion = inputs[0].count == 3
            durationInputs[0].setNumberOfItems(1)
            inputs[0] = [timeInput, durationInputs[0]]
            formView.reloadItems(at: [IndexPath(item: 1, section: 0)])
            if needsDeletion {
                formView.deleteItems(at: [IndexPath(item: 2, section: 0)])
            }
        } else if item == 1 {
            let needsDeletion = inputs[0].count == 3
            durationInputs[1].setNumberOfItems(1)
            inputs[0] = [timeInput, durationInputs[1]]
            formView.reloadItems(at: [IndexPath(item: 1, section: 0)])
            if needsDeletion {
                formView.deleteItems(at: [IndexPath(item: 2, section: 0)])
            }
        } else {
            if inputs[0].count == 3 { return }
            durationInputs.forEach { $0.setNumberOfItems(2) }
            inputs[0] = [timeInput, durationInputs[0], durationInputs[1]]
            formView.reloadItems(at: [IndexPath(item: 1, section: 0)])
            formView.insertItems(at: [IndexPath(item: 2, section: 0)])
        }
    }
}

// MARK: - DiaperForm
class DiaperForm: TrackerForm, FormUpdateSupport {
    
    // MARK: - Properties
    
    private var timeInput: TimeInput!
    private var typeInput: TypeInput!
    private var memoInput: MemoInput!
    private var colorInput: ColorInput!
    private var viscosityInput: ViscosityInput!
    
    
    // MARK: - Methods
    
    override func setup() {
        if let diaper = record.value as? Diaper {
            let t = diaper.started, s = diaper.typeIndex, m = diaper.memo, c = diaper.colorIndex, v = diaper.viscosity
            
            timeInput = TimeInput(date: t, delegate: self)
            typeInput = TypeInput(options: "diaper_type".list(count: 3).local, selected: s) {
                [unowned self] in self.updateView($0)
            }
            memoInput = MemoInput(text: m, delegate: self)
            colorInput = ColorInput(selected: c) {
                [unowned self] in self.colorDidChanged($0)
            }
            let color = colorInput.inputView.selectedColor
            viscosityInput = ViscosityInput(viscosity: v, color: color)
            
            if s == 0 {
                inputs = [[timeInput], [typeInput], [memoInput]]
            } else {
                inputs = [[timeInput], [typeInput, viscosityInput, colorInput], [memoInput]]
            }
            
            formInputs = [timeInput.inputView, memoInput.inputView]
        }
    }
    
    override func save(for child: Child) {
        let t = timeInput.value, s = typeInput.value, c = colorInput.value, v = viscosityInput.value, m = memoInput.value
        realm.update(record) {
            if let diaper = $0 as? Diaper {
                diaper.started = t
                diaper.typeIndex = s
                diaper.colorIndex = c
                diaper.viscosity = v
                diaper.memo = m
            }
        }
    }
    
    func batchUpdates(_ item: Int) {
        if item == 0 {
            if inputs[1].count == 1 { return }
            inputs[1] = [typeInput]
            formView.deleteItems(at: [IndexPath(item: 1, section: 1),
                                      IndexPath(item: 2, section: 1)])
        } else {
            if inputs[1].count == 3 { return }
            inputs[1] = [typeInput, viscosityInput, colorInput]
            formView.insertItems(at: [IndexPath(item: 1, section: 1),
                                      IndexPath(item: 2, section: 1)])
        }
    }
    
    func colorDidChanged(_ sender: ColorSelector) {
        viscosityInput.inputView.tintColor = sender.selectedColor
    }
}

// MARK: - GrowthForm
class GrowthForm: TrackerForm {
    
    // MARK: - Properties
    
    private var timeInput: TimeInput!
    private var heightInput: MeasureInput!
    private var weightInput: MeasureInput!
    private var headInput: MeasureInput!
    private var memoInput: MemoInput!
    
    
    // MARK: - Methods
    
    override func setup() {
        if let growth = record.value as? Growth {
            let t = growth.started, ht = growth.height, wt = growth.weight, hd = growth.head, m = growth.memo
            let lu = LengthUnit.init(rawValue: growth.lengthUnit)!, wu = WeightUnit.init(rawValue: growth.weightUnit)!
            
            timeInput = TimeInput(date: t, delegate: self)
            heightInput = MeasureInput(name: "height", value: (ht, lu), max: (200, .cm), step: 0.1, unit: LengthUnit.storedType, delegate: self)
            weightInput = MeasureInput(name: "weight", value: (wt, wu), max: (100, .kg), step: 0.1, unit: WeightUnit.storedType, delegate: self)
            headInput = MeasureInput(name: "head", value: (hd, lu), max: (100, .cm), step: 0.1, unit: LengthUnit.storedType, delegate: self)
            memoInput = MemoInput(text: m, delegate: self)
            
            inputs = [[timeInput], [heightInput,weightInput,headInput], [memoInput]]
            formInputs = [timeInput.inputView,
                          heightInput.inputView,
                          weightInput.inputView,
                          headInput.inputView,
                          memoInput.inputView]
        }
    }
    
    override func save(for child: Child) {
        let t = timeInput.value, ht = heightInput.value, wt = weightInput.value, hd = headInput.value, m = memoInput.value
        realm.update(record) {
            if let growth = $0 as? Growth {
                growth.started = t
                growth.height = ht
                growth.weight = wt
                growth.head = hd
                growth.lengthUnit = LengthUnit.storedType.rawValue
                growth.weightUnit = WeightUnit.storedType.rawValue
                growth.memo = m
            }
        }
    }
}
