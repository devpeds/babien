//
//  StatsViewController.swift
//  Babien
//
//  Created by SeungHoon Jung on 2018. 3. 18..
//  Copyright © 2018년 SeungHoon Jung. All rights reserved.
//

import UIKitExtension
import RealmSwift
import Charts

// MARK: - RecordType

extension RecordType {
    func statsDataSource(records: Results<AnyRecord>) -> StatsDataSource {
        switch self {
        case .sleep: return SleepStatsDataSource(records: records)
        case .breast: return BreastStatsDataSource(records: records)
        case .bottle: return BottleStatsDataSource(records: records)
        case .meal: return MealStatsDataSource(records: records)
        case .pump: return PumpStatsDataSource(records: records)
        case .diaper: return DiaperStatsDataSource(records: records)
        case .growth: return GrowthChartDataSource(records: records)
        }
    }
}


// MARK: - Controller(SuperClass)

class StatsViewController: ViewController, ChartControlDelegate, IAxisValueFormatter {

    // MARK: - Properties
    
    var child: Child!
    var type: RecordType!
    var source: StatsDataSource!
    
    
    // MARK: - Setup
    
    override func setupAfterLoad() {
        title = type.title.local
        
        let records = child.records.filter("typeName = %@", type.title).sorted(byKeyPath: "started")
        source = type.statsDataSource(records: records)
    }
    
    
    // MARK: - ChartControlDelegate
    
    func chartType() -> ChartControl.ChartType {
        fatalError("This method must be override")
    }
    
    func setup(chart: ChartViewBase, data: ChartData?) {
        chart.noDataText = "No Data".local
        chart.noDataTextColor = .darkGray
        chart.noDataFont = UIFont.boldSystemFont(ofSize: 16)
        
        chart.chartDescription?.enabled = false
        chart.legend.enabled = false
    }
    
    
    // MARK: - IAxisValueFormatter
    
    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        fatalError("This method must be override")
    }
    
}
