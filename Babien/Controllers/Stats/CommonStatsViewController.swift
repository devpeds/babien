//
//  CommonStatsViewController.swift
//  Babien
//
//  Created by SeungHoon Jung on 2018. 4. 18..
//  Copyright © 2018년 SeungHoon Jung. All rights reserved.
//

import UIKitExtension
import Charts

// MARK: - Controller

class CommonStatsViewController: StatsViewController {
    
    // MARK: - IBOutlets && UI Components
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var dateStepper: DateStepper!
    
    
    // MARK: - Properties
    
    var commonSource: CommonStatsDataSource { return source as! CommonStatsDataSource }
    
    
    // MARK: - Setup
    
    override func setupAfterLoad() {
        super.setupAfterLoad()
        dateStepper.setDate(now: Date(), max: Date(), min: child.dob.thisWeek.sunday)
        dateStepper.displayType = .weekly
        dateStepper.addTarget(self, action: #selector(stepperDidChanged(_:)), for: .valueChanged)
        let dates = (dateStepper.startDate, dateStepper.endDate)
        commonSource.setData(begin: dates.0, end: dates.1)
    }
    
    
    // MARK: - ChartControlDelegate && IAxisValueFormatter
    
    override func chartType() -> ChartControl.ChartType {
        return .bar
    }
    
    override func setup(chart: ChartViewBase, data: ChartData?) {
        super.setup(chart: chart, data: data)
        chart.isUserInteractionEnabled = false
        
        chart.data = data
        chart.data?.setDrawValues(false)
        chart.data?.notifyDataChanged()
        chart.notifyDataSetChanged()
        
        guard let bar = chart as? BarChartView else { return }
        
        bar.xAxis.valueFormatter = self
        bar.xAxis.labelPosition = .bottom
        bar.xAxis.labelFont = UIFont.boldSystemFont(ofSize: 12)
        bar.xAxis.drawGridLinesEnabled = false
        
        bar.leftAxis.axisMinimum = 0
        bar.leftAxis.labelCount = 5
        bar.leftAxis.drawGridLinesEnabled = false
        bar.leftAxis.drawAxisLineEnabled = false
        bar.leftAxis.drawGridLinesEnabled = true
        bar.leftAxis.drawBottomYLabelEntryEnabled = false
        bar.leftAxis.axisMaximum = (floor((data?.yMax ?? 0) / 5) + 1) * 5
        
        bar.rightAxis.axisMinimum = 0
        bar.rightAxis.drawLabelsEnabled = false
        bar.rightAxis.drawAxisLineEnabled = false
        bar.rightAxis.drawGridLinesEnabled = false
    }
    
    func animateDuration() -> TimeInterval {
        return 1.0
    }
    
    override func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        return "dow\(Int(value))".local
    }
    
    
    // MARK: - IBActions && Selectors
    
    @objc func stepperDidChanged(_ sender: DateStepper) {
        commonSource.setData(begin: sender.startDate, end: sender.endDate)
        tableView.reloadData()
    }
}


// MARK: - DataSource && Delegate

extension CommonStatsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return commonSource.data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: StatsCell = tableView.dequeueCell()
        cell.chartView.delegate = self
        cell.data = commonSource.data[indexPath.row]
        return cell
    }
}
