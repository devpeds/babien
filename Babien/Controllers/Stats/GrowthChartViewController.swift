//
//  GrowthChartViewController.swift
//  Babien
//
//  Created by SeungHoon Jung on 2018. 4. 18..
//  Copyright © 2018년 SeungHoon Jung. All rights reserved.
//

import UIKitExtension
import Charts

class GrowthChartViewController: StatsViewController {
    
    // MARK: - IBOutlets && UI Components
    
    @IBOutlet weak var menuBar: TabBar!
    @IBOutlet weak var chartView: ChartControl!
    
    
    // MARK: - Properties
    
    var gcSource: GrowthChartDataSource { return source as! GrowthChartDataSource }
    
    
    // MARK: - Setup
    
    override func setupAfterLoad() {
        super.setupAfterLoad()
        menuBar.setItems(titles: ["height", "weight", "head"].local)
        menuBar.font = UIFont.boldSystemFont(ofSize: 16)
        menuBar.addTarget(self, action: #selector(menuDidChanged(_:)), for: .valueChanged)
        gcSource.setOption(self.child)
        gcSource.setData(type: menuBar.selectedItem)
        chartView.delegate = self
        chartView.data = gcSource.data
    }
    
    
    // MARK: - ChartControlDelegate && IAxisFormatter
    
    override func chartType() -> ChartControl.ChartType {
        return .line
    }
    
    override func setup(chart: ChartViewBase, data: ChartData?) {
        super.setup(chart: chart, data: data)
        
        chart.data = data
        chart.data?.notifyDataChanged()
        chart.notifyDataSetChanged()
        
        guard let line = chart as? LineChartView else { return }
        
        line.scaleXEnabled = false
        line.scaleYEnabled = false
        
        line.xAxis.labelCount = 12
        line.xAxis.labelPosition = .bottom
        line.xAxis.valueFormatter = self
        line.xAxis.drawGridLinesEnabled = false

        line.rightAxis.enabled = false
        line.rightAxis.drawGridLinesEnabled = false
        
        line.leftAxis.labelCount = 10
        line.leftAxis.axisMinimum = gcSource.min
        line.leftAxis.axisMaximum = gcSource.max
        line.leftAxis.valueFormatter = MeasureFormatter(type: menuBar.selectedItem)
        line.leftAxis.drawLabelsEnabled = true
        line.leftAxis.drawGridLinesEnabled = false
        
        let marker = GrowthChartMarker(color: UIColor(hex6: 0xf4f4f4),
                                       font: UIFont.boldSystemFont(ofSize: 18),
                                       textColor: UIColor.main,
                                       insets: UIEdgeInsets(top: 8, left: 8, bottom: 20, right: 8))
        marker.chartView = line
        marker.minimumSize = CGSize(width: 80, height: 45)
        line.marker = marker
    }
    
    override func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        return "\(Int(value*12/365)) mo"
    }
    
    
    
    // MARK: - IBActions && Selectors
    
    @objc func menuDidChanged(_ sender: TabBar) {
        gcSource.setData(type: sender.selectedItem)
        chartView.data = gcSource.data
    }
}


// MARK: - AxisFormatter

class MeasureFormatter: NSObject, IAxisValueFormatter {
    
    init(type: Int) {
        super.init()
        self.type = type
    }
    
    var type: Int = 0
    
    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        if type == 1 {
            let unit = WeightUnit.storedType
            let v = WeightUnit.kg.convert(value, to: unit)
            return String(format: "%.1f%@", v, unit.value)
        }
        let unit = LengthUnit.storedType
        let v = LengthUnit.cm.convert(value, to: unit)
        return String(format: "%.1f%@", v, unit.value)
    }
}
