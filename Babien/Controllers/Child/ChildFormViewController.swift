//
//  ChildFormViewController.swift
//  Babien
//
//  Created by SeungHoon Jung on 2018. 3. 18..
//  Copyright © 2018년 SeungHoon Jung. All rights reserved.
//

import UIKitExtension

class ChildFormViewController: TableViewController {
    
    var child: Child?
    
    var isFirstChild: Bool = false
    
    // MARK: - Components
    
    @IBOutlet weak var profileView: ProfileImageView!
    
    fileprivate lazy var genderSelector: SegmentedControl = {
        let sc = SegmentedControl(titles: "babyform_sex".list(count: 2).local)
        sc.backgroundColor = .clear
        sc.thumbColor = .clear
        sc.textColor = .lightGray
        sc.selectedTextColor = .main
        sc.font = UIFont.boldSystemFont(ofSize: 18)
        return sc
    }()
    
    fileprivate lazy var nameTextField: FormTextField = { [unowned self] in
        let tf = FormTextField(inputType: .name, placeholder: "babyform_name".local)
        tf.delegate = self
        tf.layout = self
        return tf
    }()
    
    fileprivate lazy var dobTextField: DateFormTextField = { [unowned self] in
        let tf = DateFormTextField(date: Date(), placeholder: "babyform_dob".local)
        tf.dateDelegate = self
        tf.delegate = self
        tf.layout = self
        return tf
    }()
    
    fileprivate lazy var dueTextField: DateFormTextField = { [unowned self] in
        let tf = DateFormTextField(date: Date(), placeholder: "babyform_due".local)
        tf.dateDelegate = self
        tf.delegate = self
        tf.layout = self
        return tf
    }()
    
    fileprivate lazy var toolbar: FormToolbar = { [unowned self] in
        let tb = FormToolbar(inputs: [self.nameTextField,
                                      self.dobTextField,
                                      self.dueTextField])
        tb.setButtonsTintColor(.darkGray)
        return tb
    }()
    
    private lazy var imagePicker: UIImagePickerController = {
        let picker = UIImagePickerController()
        picker.delegate = self
        return picker
    }()
    
    private var alertActions: [UIAlertAction] {
        let cancelAction = UIAlertAction(title: "Cancel".local, style: .cancel, handler: nil)
        let cameraAction = UIAlertAction(title: "action_photo0".local, style: .default) {
            [unowned self] (ac) in
            self.imagePicker.sourceType = .camera
            self.present(self.imagePicker, animated: true, completion: nil)
        }
        let albumAction = UIAlertAction(title: "action_photo1".local, style: .default) {
            [unowned self] (ac) in
            self.imagePicker.sourceType = .photoLibrary
            self.present(self.imagePicker, animated: true, completion: nil)
        }
        let removeAction = UIAlertAction(title: "action_photo2".local, style: .destructive) {
            [unowned self] (ac) in self.profileView.image = nil
        }
        
        cameraAction.isEnabled = UIImagePickerController.isSourceTypeAvailable(.camera)
        albumAction.isEnabled = UIImagePickerController.isSourceTypeAvailable(.photoLibrary)
        
        return [cancelAction, cameraAction, albumAction, removeAction]
    }
    
    
    // MARK: - Setup
    
    override func setupAfterLoad() {
        title = (child == nil ? "babyform_title" : "Edit").local

        profileView.accessoryImage = #imageLiteral(resourceName: "camera")
        profileView.addTarget(target: self, action: #selector(presentAC), for: .touchUpInside)
        
        let item = UIBarButtonItem(image: #imageLiteral(resourceName: "check"), style: .done, target: self, action: #selector(save))
        item.tintColor = .main
        navigationItem.rightBarButtonItem = item
        
        // fetch data
        
        if let child = self.child {
            profileView.image = child.profile?.image
            genderSelector.selectItem(child.gender)
            nameTextField.text = child.name
            dobTextField.date = child.dob
            dobTextField.displayText()
            dueTextField.date = child.dueDate
            dueTextField.displayText()
        }
    }
    
    override func setupBeforAppear() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func setupBeforeDisappear() {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    
    // MARK: - IBActions && Selectors
    
    @objc func save() {
        if nameTextField.text!.isEmpty {
            error("alert_name".local)
        } else if dobTextField.text!.isEmpty {
            error("alert_birthday".local)
        } else {
            let profile = profileView.image
            let gender = genderSelector.selectedItem
            let name = nameTextField.text!
            let dob = dobTextField.date
            let due = dueTextField.date
            
            let realm = RealmManager.shared
            if let child = self.child {
                realm.update(child, name: name, gender: gender, dob: dob, due: due, image: profile)
            } else {
                let _ = realm.createChild(name: name, gender: gender, dob: dob, due: due, profile: profile, isSelected: isFirstChild)
            }
        }
        
        if isFirstChild {
            dismiss(animated: true, completion: nil)
        } else {
            navigationController?.popViewController(animated: true)
        }
    }
    
    @objc func presentAC() {
        let ac = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        for action in self.alertActions {
            if action.title != "action_photo2".local || profileView.image != nil {
                ac.addAction(action)
            }
        }
        self.present(ac, animated: true, completion: nil)
    }
    
    @objc func keyboardWillShow(_ notify: Notification) {
        if let info = notify.userInfo {
            let keyboardInfo = info[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue
            let height = keyboardInfo.cgRectValue.height
            let insets = UIEdgeInsets(top: 0, left: 0, bottom: height, right: 0)
            tableView.contentInset = insets
            tableView.scrollIndicatorInsets = insets
        }
    }
    
    @objc func keyboardWillHide(_ notify: Notification) {
        tableView.contentInset = .zero
        tableView.scrollIndicatorInsets = .zero
    }
    
    
    // MARK: - DataSource && Delegate
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: ChildFormCell = tableView.dequeueCell()
        cell.input = [genderSelector, nameTextField, dobTextField, dueTextField][indexPath.row]
        cell.icon = [#imageLiteral(resourceName: "gender"), #imageLiteral(resourceName: "profile"), #imageLiteral(resourceName: "cake"), #imageLiteral(resourceName: "calendar")][indexPath.row]
        return cell
    }
    
}


// MARK: - TextFieldDelegate

extension ChildFormViewController: UITextFieldDelegate, FormTextFieldLayout, DateFormTextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        toolbar.update()
        
        if let dateTextField = textField as? DateFormTextField {
            dateTextField.displayText()
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField === dobTextField {
            dueTextField.date = dobTextField.date
            dueTextField.displayText()
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        toolbar.goForward()
        return true
    }
    
    func customize(_ textField: FormTextField) {
        textField.textAlignment = .right
        textField.textColor = .darkGray
        textField.font = UIFont.boldSystemFont(ofSize: 18.0)
    }
    
    func setDatePicker(_ datePicker: UIDatePicker) {
        datePicker.datePickerMode = .date
        // TODO: - add maximum date
        datePicker.maximumDate = Date()
    }
    
    func displayText(textField: FormTextField, datePicker: UIDatePicker) -> String {
        return DateFormat.string(from: datePicker.date, type: .daily)
    }
}


// MARK: - UIImagePickerControllerDelegate

extension ChildFormViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        navigationController.customize(color: .white)
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
// Local variable inserted by Swift 4.2 migrator.
let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)

        if let image = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage {
            self.profileView.image = image
        }
        self.dismiss(animated: true, completion: nil)
    }
    
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
	return input.rawValue
}
