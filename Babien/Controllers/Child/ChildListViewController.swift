//
//  ChildListViewController.swift
//  Babien
//
//  Created by SeungHoon Jung on 2018. 3. 18..
//  Copyright © 2018년 SeungHoon Jung. All rights reserved.
//

import UIKitExtension
import RealmSwift


// MARK: - Controller

class ChildListViewController: TableViewController, RealmNotification {
    deinit {
        notification?.invalidate()
    }
    
    // MARK: - Properties
    
    let manager = RealmManager.shared
    
    var notification: NotificationToken?
    var babies: Results<Child>!
    
    
    // MARK: - Setup
    
    override func setupAfterLoad() {
        setNavBar()
        setList()
    }
    
    func setNavBar() {
        let item = UIBarButtonItem(image: #imageLiteral(resourceName: "cancel"), style: .done, target: self, action: #selector(cancel))
        navigationItem.rightBarButtonItem = item
        navigationItem.backBarButtonItem = UIBarButtonItem()
        title = "babylist_title".local
    }
    
    func setList() {
        babies = manager.read(Child.self).sorted(byKeyPath: "dob")
        notification = babies.observe {
            [unowned self] in self.update($0)
        }
    }
    
    
    // MARK: - IBActions && Selectors
    
    @objc func cancel() {
        if self.manager.selectedChild().first == nil {
            return error("alert_selection".local)
        }
        dismiss(animated: true, completion: nil)
    }
    
    
    // MARK: - DataSource && Delegate
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 { return babies.count }
        return 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell: ChildListCell = tableView.dequeueCell()
            cell.child = babies[indexPath.row]
            return cell
        }
        let cell: AddNewCell = tableView.dequeueCell()
        cell.iconView.image = #imageLiteral(resourceName: "profile")
        cell.iconView.tintColor = .lightGray
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)!
        beaver.verbose("\(String(describing: cell.reuseIdentifier!)) at \(indexPath) did select")
        if indexPath.section == 0 {
            manager.select(child: self.babies[indexPath.row])
            dismiss(animated: true, completion: nil)
        } else {
            let vc: ChildFormViewController = UIStoryboard.viewController(from: .child)
            navigationController?.pushViewController(vc, animated: true)
        }
        cell.isSelected = false
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return indexPath.section == 0
    }

    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let child = self.babies[indexPath.row]
        let delete = UITableViewRowAction(style: .default, title: "Delete".local) { [unowned self] (ac, idx) in
            self.confirm("alert_delete".local,
                         okTitle: "Delete".local,
                         okHandler: {
                            [unowned self] (ac) in self.manager.delete(child: child)
                        },
                         cancelTitle: "Cancel".local)
        }
        
        let edit = UITableViewRowAction(style: .normal, title: "Edit".local) { [unowned self] (ac, idx) in
            let vc: ChildFormViewController = UIStoryboard.viewController(from: .child)
            vc.child = child
            self.navigationController?.pushViewController(vc, animated: true)
            self.setEditing(false, animated: true)
        }

        return [delete, edit]
    }
}
