//
//  HomeViewController+CollectionView.swift
//  Babien
//
//  Created by SeungHoon Jung on 2018. 4. 5..
//  Copyright © 2018년 SeungHoon Jung. All rights reserved.
//

import UIKitExtension

// MARK: - CollectionViewDataSource && Delegate

extension HomeViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return menu.subMenus.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: MenuButton = collectionView.dequeueCell(for: indexPath)
        cell.type = menu.subMenus[indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let type = menu.subMenus[indexPath.item]
        switch menu {
        case .tracker:
            tracker(type: type)
        case .stopwatch:
            stopwatch(type: type)
        case .stats:
            statistics(type: type)
        }
        closeMenu()
    }
    
}
