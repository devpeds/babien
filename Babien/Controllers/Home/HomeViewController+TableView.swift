//
//  HomeViewController+TableView.swift
//  Babien
//
//  Created by SeungHoon Jung on 2018. 4. 5..
//  Copyright © 2018년 SeungHoon Jung. All rights reserved.
//

import UIKitExtension

// MARK: - TableViewDataSouce && Delegate

extension HomeViewController {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) as? EventCell {
            pushTrackerViewController(with: cell.record)
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let record = source.items[indexPath.section][indexPath.row]
        let delete = UITableViewRowAction(style: .default, title: "Delete".local) { [unowned self] (ac, idx) in
            self.confirm("alert_delete".local, okTitle: "Delete".local,
                         okHandler: {
                            [unowned self] (ac) in self.realm.delete(record: record)
                        },
                         cancelTitle: "Cancel".local)
        }
        return [delete]
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if let date = source.sections[section].date {
            return DateFormat.string(from: date, type: .daily)
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let header = view as! UITableViewHeaderFooterView
        header.backgroundColor = UIColor(hex6: 0xFDF828)
        header.textLabel?.textColor = .main
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.section == 0 ? 170 : 100
    }
    
    // MARK: ScrollDelegate
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        if let tv = scrollView as? UITableView {
            closeMenu()
            let y = tv.panGestureRecognizer.translation(in: tv).y
            if y < 0 {
                if top.constant != -230 {
                    UIView.animate(withDuration: 0.3, animations: {
                        self.top.constant = -230
                        self.view.layoutIfNeeded()
                    })
                }
            } else {
                if top.constant == -230 {
                    UIView.animate(withDuration: 0.3, animations: {
                        self.top.constant = 0
                        self.view.layoutIfNeeded()
                    })
                }
            }
            
        }
    }
}
