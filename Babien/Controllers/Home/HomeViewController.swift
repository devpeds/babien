//
//  HomeViewController.swift
//  Babien
//
//  Created by SeungHoon Jung on 2018. 3. 16..
//  Copyright © 2018년 SeungHoon Jung. All rights reserved.
//

import UIKitExtension
import RealmSwift

// MARK: - HomeViewController

class HomeViewController: TableViewController, RealmNotification {
    
    deinit { notification?.invalidate() }
    
    
    // MARK: - HomeMenu
    
    enum HomeMenu {
        case tracker, stopwatch, stats
        
        var subMenus: [RecordType] {
            switch self {
            case .tracker: return [.sleep, .diaper, .breast, .bottle, .pump, .meal, .growth]
            case .stopwatch: return [.sleep, .breast, .bottle, .pump, .meal]
            default: return [.sleep, .diaper, .breast, .bottle, .pump, .meal, .growth]
            }
        }
    }
    
    
    // MARK: - Properties
    
    var realm = RealmManager.shared
    var notification: NotificationToken?
    
    var selectedChild: Results<Child>!
    
    var child: Child? { return selectedChild.first }
    
    var records: Results<AnyRecord>?
    
    var source: TimeLineDataSource!
    
    var menu: HomeMenu = .tracker
    
    // MARK: - IBOutlets && Components
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var profileView: ProfileView!
    @IBOutlet weak var recentView: RecentView!
    @IBOutlet weak var menuView: UICollectionView!
    @IBOutlet weak var settingItem: UIBarButtonItem!
    @IBOutlet weak var height: NSLayoutConstraint!
    @IBOutlet weak var top: NSLayoutConstraint!
    
    
    // MARK: - Setup
    
    override func setupAfterLoad() {
        settingItem.action = #selector(presentSetting)
        profileView.addTargetWithImage(target: self, action: #selector(callChildList), for: .touchUpInside)
        profileView.addTargetWithButtons(target: self, action: #selector(buttonDidTapped(_:)), for: .touchUpInside)
        recentView.shadow(offset: (0, 1), radius: 2, opacity: 0.8, color: .lightGray)
        selectedChild = realm.selectedChild()
        records = child?.records.sorted(byKeyPath: "started", ascending: false)
        source = TimeLineDataSource(records: records)
        notification = selectedChild.observe{ self.update($0) }
        
        let refresh = UIRefreshControl()
        refresh.addTarget(self, action: #selector(refreshView(_:)), for: .valueChanged)
        tableView.refreshControl = refresh
    }
    
    override func setupBeforAppear() {
        updateProfile()
        updateRecent()
    }

    
    
    // MARK: - NotificationToken
    
    func initiateNotification() {
        beaver.debug("initiate NotificationToken")
        updateProfile()
        updateRecent()
        tableView.reloadData()
    }
    
    func updateNotification(deletions: [Int], insertions: [Int], modifications: [Int]) {
        beaver.debug("update NotificationToken")
        records = child?.records.sorted(byKeyPath: "started", ascending: false)
        self.source.reload(self.records)
        updateProfile()
        updateRecent()
        tableView.reloadData()
    }
    
    func updateProfile() {
        profileView.image = child?.profile?.image
        profileView.name = child?.name
        profileView.gender = child?.gender
        profileView.age = child?.age
    }
    
    func updateRecent() {
        let lastFeeding = records?.filter("typeName = %@ OR typeName = %@ OR typeName = %@", "Breastfeeding", "Bottlefeeding", "Meal").first?.started
        let lastDiaper = records?.filter("typeName = %@", "Diaper").first?.started
        let lastSleep = records?.filter("typeName = %@", "Sleep").first?.started
        recentView?.update(lastFeeding: lastFeeding,
                          lastDiaper: lastDiaper,
                          lastSleep: lastSleep)
    }
    
    
    // MARK: - IBActions && Selectors
    
    @objc func presentSetting() {
        let vc: SettingsViewController = UIStoryboard.viewController(from: .main)
        presentNavigationController(root: vc)
    }
    
    @objc func callChildList() {
        let vc: ChildListViewController = UIStoryboard.viewController(from: .child)
        presentNavigationController(root: vc)
    }
    
    @objc func buttonDidTapped(_ sender: UIButton) {
        profileView.buttons.forEach {
            $0.isSelected = ($0 === sender) ? !$0.isSelected : false
            $0.tintColor = $0.isSelected ? .heavyMain : .lightGray
        }
        toggleMenu(sender: sender)
    }
    
    @objc func refreshView(_ sender: UIRefreshControl) {
        source.reload(records)
        updateProfile()
        updateRecent()
        tableView.reloadData()
        sender.endRefreshing()
    }
    
    
    // MARK: - Methods for Menu
    
    func changeMenu(_ button: UIButton) {
        if !button.isSelected { return }
        
        if button === profileView.trackerButton {
            menu = .tracker
        }
        
        if button === profileView.stopwatchButton {
            menu = .stopwatch
        }
        
        if button === profileView.statsButton {
            menu = .stats
        }
        
        menuView.reloadData()
        menuView.setContentOffset(.zero, animated: false)
    }
    
    func toggleMenu(sender: UIButton) {
        UIView.animate(withDuration: 0.3, animations: {
            self.hideMenu()
        }) { (success) in
            UIView.animate(withDuration: 0.3, animations: {
                if sender.isSelected {
                    self.changeMenu(sender)
                    self.openMenu()
                }
            })
        }
    }
    
    func hideMenu() {
        height.constant = 305.0
        profileView.removeShadow()
        view.layoutIfNeeded()
    }
    
    func closeMenu() {
        profileView.buttons.forEach {
            $0.isSelected = false
            $0.tintColor = .lightGray
        }
        hideMenu()
    }
    
    func openMenu() {
        height.constant = 397.0
        profileView.shadow(offset: (0, 1), radius: 2, opacity: 0.2, color: .lightGray)
        view.layoutIfNeeded()
    }
    
    func tracker(type: RecordType) {
        pushTrackerViewController(with: realm.create(type.record(), to: child))
    }
    
    func stopwatch(type: RecordType) {
        // stopwatch already exist ask users to remake stopwatches, else create
        let sw = source.checkStopwatch(type)
        if sw.count == 0 {
            let _ = realm.create(type.record(), to: child, stopwatch: true)
        } else {
            let msg = sw.count > 1 ?
                "alert_stopwatches".local :
                String(format: "alert_stopwatch".local, sw[0].recordType.title.local)
            confirm(msg, okTitle: "Reset".local, okHandler: { (ac) in
                let realm = self.realm
                sw.forEach { realm.delete(record: $0) }
                let _ = realm.create(type.record(), to: self.child, stopwatch: true)
            }, cancelTitle: "Cancel".local)
        }
        
    }
    
    func statistics(type: RecordType) {
        var vc: StatsViewController
        switch type {
        case .growth:
            let gc: GrowthChartViewController = UIStoryboard.viewController(from: .stats)
            vc = gc
        default:
            let cc: CommonStatsViewController = UIStoryboard.viewController(from: .stats)
            vc = cc
        }
        vc.child = self.child
        vc.type = type
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func pushTrackerViewController(with record: AnyRecord) {
        let vc: TrackerViewController = UIStoryboard.viewController(from: .main)
        vc.child = self.child
        vc.record = record
        vc.type = record.recordType
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
    // MARK: - DataSource && Delegate
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        let count = records?.count
        let label = UILabel(text: "No Data".local, color: .lightGray, font: UIFont.boldSystemFont(ofSize: 18))
        label.textAlignment = .center
        label.frame = tableView.bounds
        label.text = count == 0 ? "No Data".local : ""
        tableView.backgroundView = label
        tableView.separatorStyle = .none
        
        return source.sections.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return source.sections[section].count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let record = source.items[indexPath.section][indexPath.item]
        
        switch record.numberOfStopwatches {
        case 2:
            let cell: TwoStopwatchCell = tableView.dequeueCell(for: indexPath)
            cell.record = record
            return cell
        case 1:
            let cell: OneStopwatchCell = tableView.dequeueCell(for: indexPath)
            cell.record = record
            return cell
        default:
            let cell: EventCell = tableView.dequeueCell(for: indexPath)
            cell.record = record
            return cell
        }
    }
}
