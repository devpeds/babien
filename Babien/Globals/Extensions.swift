//
//  Extensions.swift
//  Babien
//
//  Created by SeungHoon Jung on 2018. 3. 16..
//  Copyright © 2018년 SeungHoon Jung. All rights reserved.
//

import Foundation
import UIKitExtension

// MARK: - UIStoryboard

extension UIStoryboard {
    static func viewController<T: UIViewController>(from type: StoryboardType) -> T {
        return UIStoryboard.viewController(fromStoryboard: type.rawValue)
    }
}


// MARK: - UIViewController

extension UIViewController {
    
    func addViewController(asChildViewController childViewController: UIViewController) {
        addChild(childViewController)
        
        view.addSubview(childViewController.view)
        childViewController.view.frame = view.bounds
        childViewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        childViewController.didMove(toParent: self)
    }
    
    func removeViewController(asChildViewController childViewController: UIViewController) {
        childViewController.willMove(toParent: nil)
        childViewController.view.removeFromSuperview()
        childViewController.removeFromParent()
    }
}

// MARK: - UINavigationController

extension UINavigationController {
    func customize(color: UIColor? = nil) {
        navigationBar.tintColor = .lightGray
        navigationBar.titleTextAttributes = [
            NSAttributedString.Key.foregroundColor: UIColor.darkGray,
            NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 24)
        ]
        navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationBar.shadowImage = UIImage()
        navigationBar.isTranslucent = false
        navigationBar.barTintColor = color
    }
}

