//
//  Enums.swift
//  Babien
//
//  Created by SeungHoon Jung on 2018. 3. 16..
//  Copyright © 2018년 SeungHoon Jung. All rights reserved.
//

import Foundation
import UIKitExtension

// MARK: - StoryboardType

enum StoryboardType: String {
    case main = "Main"
    case child = "Child"
    case tracker = "Tracker"
    case stats = "Statistics"
}

// MARK: - DateDisplayType

enum DateDisplayType: Int {
    case daily = 0, weekly, monthly
}

