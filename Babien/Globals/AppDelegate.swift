//
//  AppDelegate.swift
//  Babien
//
//  Created by SeungHoon Jung on 2018. 3. 16..
//  Copyright © 2018년 SeungHoon Jung. All rights reserved.
//

import UIKit
import RealmSwift
import SwiftyBeaver
import GoogleMobileAds

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        // set logging
        beaver.info("The app did finish launching")
        
        // Initialize the Google Mobile Ads SDK.
        GADMobileAds.configure(withApplicationID: "ca-app-pub-3182275476710852~6833724363")
        
        // move option values from standard to shared
        moveValueToShared(VolumeUnit.self)
        moveValueToShared(LengthUnit.self)
        moveValueToShared(WeightUnit.self)
        
        // test
//        let test = TestOnly.shared
//        test.initiate()
//        test.screenShot()
        
        
        // hijacking root view controller
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.makeKeyAndVisible()
        window?.rootViewController = MainNavigationController()
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        beaver.info("The app is about to move from active to inactive")
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        beaver.info("The application did enter background")
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        beaver.info("The application will enter foreground")
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        beaver.info("The application did become active")
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        beaver.info("The application is about to terminate")
    }

    
    func moveValueToShared<T: KeyValueSettable>(_ key: T.Type) {
        if UserDefaults.standard.object(forKey: T.keyName) != nil {
            let v = UserDefaults.standard.type(forOption: key)
            UserDefaults.standard.remove(key)
            UserDefaults.shared.set(v)
        }
    }

}

