//
//  UserDefaults.swift
//  Babien
//
//  Created by SeungHoon Jung on 2018. 3. 16..
//  Copyright © 2018년 SeungHoon Jung. All rights reserved.
//

import Foundation

// MARK: - UserDefaults

extension UserDefaults {
    
    static let shared: UserDefaults = UserDefaults(suiteName: groupId)!
    
    static func synchronize() {
        UserDefaults.standard.synchronize()
        UserDefaults.shared.synchronize()
    }
    
    func set<T: KeyValueSettable>(_ value: T) {
        self.set(value.rawValue, forKey: T.keyName)
    }
    
    func remove<T: KeyValueSettable>(_ option: T.Type) {
        self.removeObject(forKey: T.keyName)
    }
    
    func rawValue<T: KeyValueSettable>(forOption option: T.Type) -> T.RawValue {
        return self.integer(forKey: T.keyName)
    }
    
    func type<T: KeyValueSettable>(forOption option: T.Type) -> T {
        return T.init(rawValue: self.rawValue(forOption: option))!
    }
    
    func value<T: KeyValueSettable>(forOption option: T.Type) -> T.ValueType {
        return self.type(forOption: option).value
    }
}

// MARK: Options

enum LaunchedBefore: Int, KeyValueSettable {
    typealias ValueType = Bool
    case no, yes
}

enum VolumeUnit: Int, MetricConvertable {
    typealias ValueType = String
    var m: Float { return 0.033814 }
    case ml, oz
}

enum LengthUnit: Int, MetricConvertable {
    typealias ValueType = String
    var m: Float { return 0.39370 }
    case cm, inch
}

enum WeightUnit: Int, MetricConvertable {
    typealias ValueType = String
    var m: Float { return 2.2046 }
    case kg, lbs
}


// MARK: - KeyValueSettable

protocol KeyValueSettable: RawRepresentable where Self.RawValue == Int {
    associatedtype ValueType
    
    static var keyName: String { get }
    static var nameList: [String] { get }
    static var storedType: Self { get }
    static var storedValue: ValueType { get }
    
    var name: String { get }
    var value: ValueType { get }
}

extension KeyValueSettable {
    static var keyName: String { return String(describing: self) }
    
    static var nameList: [String] {
        var list: [String] = [], count: Int = 0
        while true {
            guard let name = Self.init(rawValue: count)?.name else { break }
            list.append(name)
            count += 1
        }
        return list
    }
    
    static var storedType: Self  { return UserDefaults.standard.type(forOption: Self.self) }
    
    static var storedValue: ValueType { return Self.storedType.value }
    
    var name: String { return String(describing: self) }
    
}

extension KeyValueSettable where Self.ValueType == String {
    var value: String { return self.name }
}

extension KeyValueSettable where Self.ValueType == Bool {
    var value: Bool { return self.rawValue != 0 }
}


// MARK: - MetricConvertible

protocol MetricConvertable: KeyValueSettable {
    var m: Float { get }
    
    func convert(_ value: Float) -> Float
    func convert(_ value: Double) -> Double
    func convert<T:MetricConvertable>(_ value: Float, to unit: T) -> Float
    func convert<T:MetricConvertable>(_ value: Double, to unit: T) -> Double
}

extension MetricConvertable {
    func convert(_ value: Float) -> Float {
        return convert(value, to: Self.storedType)
    }
    
    func convert(_ value: Double) -> Double {
        return convert(value, to: Self.storedType)
    }
    
    func convert<T:MetricConvertable>(_ value: Float, to unit: T) -> Float {
        if self.rawValue == unit.rawValue { return value }
        return self.rawValue == 0 ? value * m : value / m
    }
    
    func convert<T:MetricConvertable>(_ value: Double, to unit: T) -> Double {
        if self.rawValue == unit.rawValue { return value }
        return self.rawValue == 0 ? value * Double(m) : value / Double(m)
    }
}

