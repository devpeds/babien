//
//  TestOnly.swift
//  Babien
//
//  Created by SeungHoon Jung on 2018. 4. 12..
//  Copyright © 2018년 SeungHoon Jung. All rights reserved.
//

import UIKitExtension
import RealmSwift
import GoogleMobileAds

class TestOnly: NSObject {
    static let shared: TestOnly = TestOnly()
    
    private override init() { super.init() }
    
    private let realm = RealmManager.shared
    
    private var babien: Child!
    
    func initiate() {
        if let url = Realm.Configuration.defaultConfiguration.fileURL {
            print(url.absoluteString)
            try! FileManager.default.removeItem(at: url)
        }
        
        UserDefaults.standard.set(VolumeUnit.ml)
        UserDefaults.standard.set(LengthUnit.cm)
        UserDefaults.standard.set(WeightUnit.kg)
        
        let dob = Date.components(year: 2018, month: 1, day: 1)!
        babien = realm.createChild(name: "test_name".local, gender: 0, dob: dob, due: dob, profile: nil, isSelected: true)
        
    }
    
    func screenShot() {
        // from 4/5 to 4/11
        // sleep
        //  - night: 10hr, day: 5hr, count: 3
        // feeding
        //  - term: 3-4hr or 5-6hr, amount: 100-160ml, duration: 15-30 min
        // diaper
        //  - 5-7 times/day
        for i in 5...11 {
            var records: [Record] = []
            let r0 = Diaper(started: date(d: i, h: 8), type: 2, color: 0, viscosity: 0, memo: nil)
            let r1 = Breastfeeding(started: date(d: i, h: 8, m: 5), left: 900, right: 900, memo: nil)
            let r2 = Diaper(started: date(d: i, h: 10, m: 10), type: 0, color: 1, viscosity: 0.5, memo: nil)
            let r3 = Breastfeeding(started: date(d: i, h: 11, m: 10), left: 600, right: 600, memo: nil)
            let r4 = Diaper(started: date(d: i, h: 12), type: 0, color: 0, viscosity: 0, memo: nil)
            let r5 = Sleep(started: date(d: i, h: 12, m: 15), duration: 5*3600, type: 0, memo: nil)
            let r6 = Pump(started: date(d: i, h: 14, m: 30), left: 600, right: 600, type: 2, amount: 120, memo: nil)
            let r7 = Bottlefeeding(started: date(d: i, h: 17), duration: 900, type: 1, amount: 140, memo: nil)
            let r8 = Diaper(started: date(d: i, h: 17, m: 50), type: 2, color: 1, viscosity: 1, memo: nil)
            let r9 = Diaper(started: date(d: i, h: 19, m: 40), type: 0, color: 2, viscosity: 0.5, memo: nil)
            let r10 = Breastfeeding(started: date(d: i, h: 21), left: 600, right: 600, memo: nil)
            let r11 = Sleep(started: date(d: i, h: 22, m: 0), duration: 10*3600, type: 1, memo: nil)
            
            
            records = [r0, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11]
            records.forEach {
                let _ = realm.create($0, to: babien)
            }
        }
        
        // Diapers
        [ Diaper(started: date(d: 11), type: 2, color: 0, viscosity: 0, memo: nil),
          Diaper(started: date(d: 10), type: 2, color: 0, viscosity: 0, memo: nil),
          Diaper(started: date(d: 10), type: 0, color: 0, viscosity: 0, memo: nil),
          Diaper(started: date(d: 8), type: 0, color: 0, viscosity: 0, memo: nil),
          Diaper(started: date(d: 8), type: 0, color: 0, viscosity: 0, memo: nil),
          Diaper(started: date(d: 8), type: 0, color: 0, viscosity: 0, memo: nil),
          Diaper(started: date(d: 8), type: 2, color: 0, viscosity: 0, memo: nil)].forEach {
            let _ = realm.create($0, to: babien)
        }
        
        
        // Today(12th)
        let r0 = Diaper(started: date(d: 12, h: 8), type: 2, color: 0, viscosity: 0, memo: nil)
        let r1 = Breastfeeding(started: date(d: 12, h: 8, m: 5), left: 900, right: 900, memo: nil)
        let r2 = Diaper(started: date(d: 12, h: 10, m: 10), type: 0, color: 1, viscosity: 0.5, memo: nil)
        let r3 = Breastfeeding(started: date(d: 12, h: 11, m: 10), left: 600, right: 600, memo: nil)
        let r4 = Diaper(started: date(d: 12, h: 11, m: 45), type: 0, color: 0, viscosity: 0, memo: nil)
//        let r5 = Sleep(started: date(d: 12, h: 12, m: 15), duration: 5*3600, type: 0, memo: nil)
        [r0, r1, r2, r3, r4].forEach {
            let _ = realm.create($0, to: babien)
        }
    }
    
    private func date(d: Int, h: Int = 0, m: Int = 0) -> Date {
        return Date.components(year: 2018, month: 4, day: d, hr: h, min: m)!
    }
}
