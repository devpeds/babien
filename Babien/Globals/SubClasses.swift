//
//  SubClasses.swift
//  Babien
//
//  Created by SeungHoon Jung on 2018. 3. 28..
//  Copyright © 2018년 SeungHoon Jung. All rights reserved.
//

import UIKitExtension
import RealmSwift

// MARK: - RealmNotification

protocol RealmNotification: class {
    func update<T>(_ change: RealmCollectionChange<T>)
    func initiateNotification()
    func updateNotification(deletions: [Int], insertions: [Int], modifications: [Int])
    func notificationError(_ error: Error)
}

extension RealmNotification where Self: UIViewController {
    func update<T>(_ change: RealmCollectionChange<T>) {
        switch change {
        case .initial(_):
            initiateNotification()
            break
        case .update(_, deletions: let d, insertions: let i, modifications: let m):
            updateNotification(deletions: d, insertions: i, modifications: m)
            break
        case .error(let e):
            notificationError(e)
            break
        }
    }
    
    func notificationError(_ error: Error) {
        beaver.error(error.localizedDescription)
        fatalError(error.localizedDescription)
    }
}

extension RealmNotification where Self: TableViewController {
    func initiateNotification() {
        self.tableView.reloadData()
    }
    
    func updateNotification(deletions: [Int], insertions: [Int], modifications: [Int]) {
        beaver.debug("default update method be executed")
        self.tableView.beginUpdates()
        self.tableView.insertRows(at: insertions.map { IndexPath(row: $0, section: 0) }, with: .automatic)
        self.tableView.deleteRows(at: deletions.map { IndexPath(row: $0, section: 0) }, with: .automatic)
        self.tableView.reloadRows(at: modifications.map { IndexPath(row: $0, section: 0) }, with: .automatic)
        self.tableView.endUpdates()
    }
}


// MARK: - ViewController

class ViewController: UIViewController {
    
    private var name: String { return "\(type(of: self))" }
    
    // MARK: - LifeCycles
    
    override func viewDidLoad() {
        super.viewDidLoad()
        beaver.verbose("\(name) did load")
        navigationController?.customize(color: view.backgroundColor)
        navigationItem.backBarButtonItem = UIBarButtonItem()
        setupAfterLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        beaver.verbose("\(name) will appear")
        setupBeforAppear()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        beaver.verbose("\(name) did appear")
        setupAfterAppear()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        beaver.verbose("\(name) will disappear")
        setupBeforeDisappear()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        beaver.verbose("\(name) did disappear")
        setupAfterDisappear()
    }
    
    // MARK: - Customization
    
    func setupAfterLoad() {}
    
    func setupBeforAppear() {}
    
    func setupAfterAppear() {}
    
    func setupBeforeDisappear() {}
    
    func setupAfterDisappear() {}
    
    
    // MARK: - Present VC with UINavigationController
    
    func presentNavigationController(root: UIViewController) {
        present(UINavigationController(rootViewController: root), animated: true, completion: nil)
    }
    
}

// MARK: - TableViewController

class TableViewController: ViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var tableView: UITableView!

    
    // MARK: - DataSource && Delegate
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        fatalError("The method must be override")
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        fatalError("The method must be override")
    }
}
