//
//  Shared.swift
//  Babien
//
//  Created by SeungHoon Jung on 2018. 5. 3..
//  Copyright © 2018년 SeungHoon Jung. All rights reserved.
//

import SwiftyBeaver
import UIKitExtension

public var beaver: SwiftyBeaver.Type = {
    let beaver = SwiftyBeaver.self
    let console = ConsoleDestination()
    console.format = "$DHH:mm:ss$d $L $M"
    beaver.addDestination(console)
    return beaver
}()

public let groupId: String = "group.com.devpeds.Babien"

public let realmPath: String = "default.realm"


// MARK: - Extensions

extension String {
    var local: String {
        return NSLocalizedString(self, comment: "")
    }
    
    func list(count: Int) -> [String] {
        return (0..<count).map { "\(self)\($0)" }
    }
}

extension Array where Iterator.Element == String {
    var local: [String] {
        return self.map { $0.local }
    }
}

extension Date {
    func string(format: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
}

extension UIColor {
    static var main: UIColor { return UIColor(hex6: 0xFB9EAB) }
    
    static var heavyMain: UIColor { return UIColor(hex6: 0xFB8697) }
    
    static var lightMain: UIColor { return UIColor(hex6: 0xFCF4F6) }
    
    static var sub: UIColor { return UIColor(hex6: 0x4DCEDF) }
    
    static var lightSub: UIColor { return UIColor(hex6: 0xBFEBFE) }
}


// MARK: - NibView

protocol NibLoadable {
    var nibName: String { get }
    
    func loadFromNib()
    func initiate()
}

extension NibLoadable where Self: UIView {
    var nibName: String { return String(describing: type(of: self)) }
    
    func loadFromNib() {
        let bundle = Bundle.init(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
        initiate()
    }
    
}

class NibView: View, NibLoadable {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadFromNib()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadFromNib()
    }
    
    func initiate() {}
}



// MARK: - DateFormat

enum DateFormat: String {
    case daily = "yyyy. MM. dd."
    case monthly = "MMMM, yyyy"
    case time = "a hh:mm"
    
    static func string(from date: Date, type: DateFormat) -> String {
        return date.string(format: type.rawValue)
    }
    
    static func string<T: BinaryFloatingPoint>(from duration: T) -> String {
        let hr = UInt(duration / 3600)
        let min = UInt(duration / 60) - hr*60
        
        return DateFormat.string(hr: hr, min: min)
    }
    
    static func string(from date: Date?) -> String {
        func s(_ i: UInt) -> String { return i > 1 ? "s" : "" }
        if let time = date?.timeIntervalSinceNow {
            let hr = UInt(-time / 3600)
            let min = UInt(-time / 60) - hr*60
            let day = hr / 24
            
            var df: Int = 1, args: [CVarArg] = []
            switch day {
            case 0:
                if hr > 0 { args.append(DateFormat.string(hr: hr, min: 0)) }
                else if min > 0 { args.append(DateFormat.string(hr: hr, min: min)) }
                else { df = 0 }
            case 1:
                df = 2
            case 2, 3:
                df = 3
                args.append(day)
            default:
                df = 4
            }
            
            return String(format: "df_recent\(df)".local, arguments: args)
        }
        return "N/A"
    }
    
    private static func string(hr: UInt, min: UInt) -> String {
        if hr > 0 {
            return min > 0 ?
                String(format: "%u %@ %u %@", hr, "hr".local, min, "min".local) :
                String(format: "%u %@", hr, "hr".local)
        }
        return String(format: "%u %@", min, "min".local)
    }
    
    static func stopwatch(time: TimeInterval, showHr: Bool) -> String {
        let hr  = showHr ? UInt(time / 3600) : 0
        let min = UInt(time / 60) - 60*hr
        let sec = UInt(time.truncatingRemainder(dividingBy: 60))
        
        if hr > 0 {
            return String(format: "%02d:%02d:%02d", hr, min, sec)
        }
        return String(format: "%02d:%02d", min, sec)
    }
}


// MARK: - MeasureFormat

enum MeasureFormat {
    case volume(unit: VolumeUnit)
    case length(unit: LengthUnit)
    case weight(unit: WeightUnit)
    
    // MARK: - Methods
    
    func convert(value: Float) -> Float {
        switch self {
        case .volume(let unit): return unit.convert(value)
        case .length(let unit): return unit.convert(value)
        case .weight(let unit): return unit.convert(value)
        }
    }
    
    func string(_ value: Float) -> String {
        var format: String, unitText: String
        switch self {
        case .volume(_):
            format = VolumeUnit.storedType == .ml ? "%0.0f" : "%0.1f"
            unitText = VolumeUnit.storedValue
        case .length(_):
            format = "%0.1f"
            unitText = LengthUnit.storedValue
        case .weight(_):
            format = "%0.1f"
            unitText = WeightUnit.storedValue
        }
        return String(format: "\(format)\(unitText)", convert(value: value))
    }
}


// MARK: - RecordType

enum RecordType: Int {
    case sleep = 0, breast, bottle, meal, pump, diaper, growth
    
    init?(title: String) {
        guard let rawValue = RecordType.titleLists.index(of: title) else { return nil }
        self.init(rawValue: rawValue)
    }
    
    
    // MARK: - Properties
    
    var title: String { return RecordType.titleLists[self.rawValue] }
    
    var icon: UIImage? {
        return [#imageLiteral(resourceName: "sleep"), #imageLiteral(resourceName: "breast"), #imageLiteral(resourceName: "bottle"), #imageLiteral(resourceName: "meal"), #imageLiteral(resourceName: "pump"), #imageLiteral(resourceName: "diaper"), #imageLiteral(resourceName: "growth")][self.rawValue]
    }
    
    var color: UIColor {
        let hex6: UInt32 = [0xB6D126,
                            0xFB992B,
                            0x7E7BFF,
                            0xFFE919,
                            0x67A0FF,
                            0x7E5D1E,
                            0xD4BCFF][self.rawValue]
        
        return UIColor(hex6: hex6)
    }
    
    
    // MARK: - Methods
    
    func record() -> Record {
        switch self {
        case .sleep: return Sleep()
        case .breast: return Breastfeeding()
        case .bottle: return Bottlefeeding()
        case .meal: return Meal()
        case .pump: return Pump()
        case .diaper: return Diaper()
        case .growth: return Growth()
        }
    }
    
    // MARK: - Statics
    
    static var titleLists: [String] {
        return ["Sleep", "Breastfeeding", "Bottlefeeding", "Meal", "Pump", "Diaper", "Growth"]
    }
    
    static var count: Int { return RecordType.titleLists.count }
    
    static func color(from record: AnyRecord) -> UIColor {
        guard let type = RecordType.init(title: record.typeName) else {
            fatalError("cannot find \(record.typeName) from RecordType.")
        }
        return type.color
    }
    
}
